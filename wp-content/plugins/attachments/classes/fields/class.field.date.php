<?php

/**
 * Attachments date field
 *
 * @package Attachments
 * @subpackage Main
 */

class Attachments_Field_Date extends Attachments_Field {

    private $data = [];

    /**
     * Constructor
     * @param string $name  Field name
     * @param string $label Field label
     * @param mixed $value Field value
     */
    function __construct( $name = 'name', $label = 'Name', $value = null, $meta = array() ) {
        if(isset($meta['data'])) {
            $this->data = $meta['data'];
            unset($meta['data']);
        }

        parent::__construct( $name, $label, $value, $meta );
    }



    /**
     * Outputs the HTML for the field
     * @param  Attachments_Field $field The field object
     * @return void
     */
    function html( $field ) {
        $data = $this->data;
        if(is_array($this->data)) {
            $data = '';
            foreach($this->data as $key => $value) {
                $data .= ' data-'.$key.'="'.$value.'"';
            }
        }
        $uid = 'cal_'.md5(time()).rand();
    ?>
        <input type="text" data-field="date" name="<?php esc_attr_e($field->field_name); ?>" id="<?= $uid ?>" class="attachments attachments-field attachments-field-<?php esc_attr_e($field->field_name); ?> attachments-field-<?php esc_attr_e($field->field_id); ?>" value="<?php esc_attr_e($field->value); ?>" data-default="<?php esc_attr_e($field->default); ?>" <?= $data ?> />
    <?php
    }



    /**
     * Filter the field value to appear within the input as expected
     * @param  string $value The field value
     * @param  Attachments_field $field The field object
     * @return string        The formatted value
     */
    function format_value_for_input( $value, $field = null  ) {
        return htmlspecialchars( $value, ENT_QUOTES );
    }



    /**
     * Fires once per field type per instance and outputs any additional assets (e.g. external JavaScript)
     * @return void
     */
    public function assets() {
        ?>
        <link rel="stylesheet" type="text/css" href="/wp-content/plugins/attachments/js/jquery.calendars/jquery.calendars.picker.css"/>
        <script type="text/javascript" src="/wp-content/plugins/attachments/js/jquery.calendars/jquery.plugin.min.js" ></script>
        <script type="text/javascript" src="/wp-content/plugins/attachments/js/jquery.calendars/jquery.calendars.js" ></script>
        <script type="text/javascript" src="/wp-content/plugins/attachments/js/jquery.calendars/jquery.calendars.plus.js" ></script>
        <script type="text/javascript" src="/wp-content/plugins/attachments/js/jquery.calendars/jquery.calendars.picker.js" ></script>
        <script type="text/javascript" src="/wp-content/plugins/attachments/js/jquery.calendars/jquery.calendars.hebrew.js" ></script>
        <script type="text/javascript" src="/wp-content/plugins/attachments/js/jquery.calendars/jquery.calendars-ru.js " ></script>
        <script type="text/javascript" src="/wp-content/plugins/attachments/js/jquery.calendars/jquery.calendars.picker-ru.js " ></script>
        <script type="text/javascript">
        jQuery(function() {
            jQuery('body').on('date:init', '[data-field="date"]', function(){
                var cal = jQuery(this).data('calendar');
                if(cal) {
                    var calendar = jQuery.calendars.instance(cal);
                }
                else {
                    var calendar = jQuery.calendars.instance();
                }
                if(calendar.regionalOptions.ru) {
                    calendar.local = calendar.regionalOptions.ru;
                }
                else {
                    calendar.local.dateFormat = 'dd.mm.YYYY';
                }
                jQuery(this).calendarsPicker({calendar: calendar, onSelect: function(dates) {
                    // Выбор целой недели
                    var wday = dates[0].dayOfWeek();
                    var begin = dates[0].newDate().add(-wday,'d');
                    var end = begin.newDate().add(6,'d');
                    jQuery(this).val(begin.formatDate()+' - '+end.formatDate())
                }});
            });
            jQuery('[data-field="date"]').trigger('date:init');
            // для выбора недели
            jQuery('.calendars-popup .calendars-month tbody tr').live('mousemove', function() { jQuery(this).find('td a').addClass('calendars-highlight'); });
            jQuery('.calendars-popup .calendars-month tbody tr').live('mouseleave', function() { jQuery(this).find('td a').removeClass('calendars-highlight'); });
        });
        </script>
        <?php
        return;
    }



    /**
     * Hook into WordPress' init action
     * @return void
     */
    function init() {
        return;
    }

}
