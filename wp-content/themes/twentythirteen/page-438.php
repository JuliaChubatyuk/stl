<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
    <script>
        $(function() {
            $('#slides').slidesjs({
                preload: true,
                preloadImage: 'img/loading.gif',
                generatePagination: false,
                navigation: false ,
                pagination: false,
                width: 675,
                height: 448
            });
        });
    </script>
<div style="width: 1229px; margin: 0 auto">
<header>
    <div class="header">
    <div class="second-level-header">See The Light</div>
    <button class="pp" onclick="popupShow('popup_donate')">Поддержать проект</button>
    </div>
</header>
    <div class="clear"></div><!-- clear -->
<main>
    <div class="main main-project">
        <img  src="/wp-content/themes/twentythirteen/images/pr-stl-logo.png" alt="pr-stl-logo" class="pr-stl-img_0"/>

        <div  class="pr-stl_text_0 pr-stl_text_1_border">21 января (10 швата) 2013 года успешно стартовал молодежный проект SeeTheLight, направленный на создание крупнейшей сети еврейских молодежных клубов не только в Украине, но и за ее пределами</div>

        <img  src="/wp-content/themes/twentythirteen/images/foto2.jpg" alt="foto2" class="pr-stl-img_2"/>
        <div  class="pr-stl_text_2">Этот проект по-настоящему уникален, т.к. это единственное на постсоветском пространстве молодежное еврейское движение галахических евреев. Кроме того, основной двигатель SeeTheLight – это сама молодежь и их желание сплочаться, дружить, менять, что-то развивать, куда-то стремиться. Это и есть сила SeeTheLight.</div>

        <div  class="pr-stl-img_1 animateme scrollme"
        data-when="enter"
        data-from=".6"
        data-to=".1"
        data-translatey="90">
            <div id="slides" class="posR">
                <img src="/wp-content/themes/twentythirteen/img/3/0.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/1.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/2.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/3.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/4.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/5.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/6.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/7.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/8.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/9.jpg" alt="" />
                <img src="/wp-content/themes/twentythirteen/img/3/10.jpg" alt="" />
                <a href="#" class="slidesjs-previous slidesjs-navigation"><i class="pr-stl_arrow_left"></i></a>
                <a href="#" class="slidesjs-next slidesjs-navigation"><i class="pr-stl_arrow_right"></i></a>
            </div>
        </div>

        <div  class="pr-stl-text_3 animateme scrollme"
        data-when="enter"
        data-from=".7"
        data-to=".1"
        data-opacity="0"
        data-translatey="100">На данный момент уже более, чем в 20 городах Украины, в 2 городах РФ и в 1 городе в Австрии действуют молодежные клубы SeeTheLight, объединенные одной целью и одной идеей.
            <br/><br/>
            Регулярно продолжают открываться новые клубы, которые мы с большой радостью принимаем в семью.
        </div>

        <div  class="pr-stl-text_4 animateme scrollme"
        data-when="enter"
        data-from=".7"
        data-to=".1"
        data-opacity="0"
        data-translatey="150">Деятельность клубов заключается в организации молодежных мероприятий различного уровня и направления. При этом, помимо объединения еврейской молодежи, проект открывает участникам новые стороны культуры и многовековых ценностей. </div>

        <div  class="pr-stl-text_5 animateme scrollme"
        data-when="enter"
        data-from=".6"
        data-to=".1"
        data-opacity="0"
        data-translatey="50">Проект предназначен для участников<br/> 18-32 лет</div>

        <img  src="/wp-content/themes/twentythirteen/images/foto3.jpg" alt="foto3" class="pr-stl-img_3 animateme scrollme"
        data-when="enter"
        data-from=".6"
        data-to=".3"
        data-translatey="50"/>

    </div>
    <div class="second-level-header">Другие проекты</div>
</main>
    <div class="clear"></div><!-- clear -->

</div>
<footer class="footer">

    <div class="pr-stl">
        <div class="pr-stl_wrapper">
        <div class="floatL">
                <a href="<?php echo get_permalink(459); ?>"><div class="pr-stl_content pr-stl_content_1"></div></a>
        </div>
       <div class="floatL">
                <a href="<?php echo get_permalink(465); ?>"><div class="pr-stl_content pr-stl_content_2"></div></a>
        </div>
        <div class="floatL">
            <a href="<?php echo get_permalink(463); ?>"><div class="pr-stl_content pr-stl_content_3"></div></a>
        </div>
        <div class="floatL">
            <a href="<?php echo get_permalink(437); ?>"><div class="pr-stl_content pr-stl_content_4"></div></a>
        </div>
        </div>
    </div>
</footer>
<script >
    $(document).ready(function(){
        if(window.innerWidth > 500){
         var script_scroll = $("<script>");
         script_scroll.attr("src", '/wp-content/themes/twentythirteen/js/jquery.scrollme.min.js')
         $(document.body).append(script_scroll);
        }
    });
</script>
<?php require_once 'popup_donate.php'; ?>
<?php get_footer();?>