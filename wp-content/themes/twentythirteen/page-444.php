<?php get_header(); ?>
<?php include_once ABSPATH.'/wp-content/themes/twentythirteen/youtube_attachments.php' ?>
<script type="text/javascript" src="/wp-content/themes/twentythirteen/js/select2/js/select2.full.min.js"></script>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/twentythirteen/js/select2/css/select2.min.css"/>
<div class="container splice-block-animation">
    <div class="static-video-page">
        <header class="header-wrapper" >
            <div class="button-wrapper">
                <a href="/?page_id=728">
                    <span class="info-button">Архив</span>
                </a>
                <a href=" /?page_id=401"><span class="info-button">На этой неделе</span></a>
            </div>
                <div class="second-level-header">В ногу со временем</div>
        </header>
        <div class="video-content ">
            <div class="video-form" >
               <div class="video-filter-title-1">Глава</div>
                <select id="video-glava" data-placeholder="Выбрать главу">
                    <option></option>
                    <?php
                    $top = 0;
                    foreach($glavas as $glava) :
                        $top += 43;
                    ?>
                        <option value="<?= $glava ?>"><?= $glava ?></option>
                    <?php
                    endforeach
                    ?>
                </select>
                <div class="video-filter-title-2">Еврейский календарь</div>
                <select id="video-dateh" >
                    <option></option>
                    <?php
                    $top = 0;
                    foreach($datehs as $dateh) :
                        $top += 43;
                    ?>
                        <option value="<?= $dateh ?>"><?= $dateh ?></option>
                    <?php
                    endforeach
                    ?>
                </select>
                <div class="video-filter-title-3">Григорианский календарь</div>
                <select id="video-dateg" >
                    <option></option>
                    <?php
                    $top = 0;
                    foreach($dategs as $dateg) :
                        $top += 43;
                    ?>
                        <option value="<?= $dateg ?>"><?= $dateg ?></option>
                    <?php
                    endforeach
                    ?>
                </select>
                <button class="pp custom" onclick="popupShow('popup_donate')">Поддержать проект</button>
            </div>
            <div class="video-container"  >
                <div id="player">
                    <iframe src="https://www.youtube.com/embed/<?= empty($videos[0]) ? '' : $videos[0]->fields->id ?>?enablejsapi=1"></iframe>
                </div>
            </div>
            <div class="video-list"  >
                <div class="video-title hide-screen" id="title"><?= empty($videos[0]) ? '' : $videos[0]->fields->title ?></div>
                <div class="controller">
                    <button class="prev" id="prev"></button>
                    <button class="next" id="next"></button>
                    <button class="update"></button>
                </div>
                <div class="video-list-wrapper"  >
                    <div id="list">
                        <?php foreach($videos as $i => $video) : ?>
                            <div
                            data-link="<?= $video->fields->link ?>"
                            data-id="<?= $video->fields->id ?>"
                            data-title="<?= $video->fields->title ?>"
                            data-glava="<?= $video->fields->glava ?>"
                            data-dateh="<?= $video->fields->dateh ?>"
                            data-dateg="<?= $video->fields->dateg ?>"
                            class="video-wrapper <?= $i==0 ? 'active' : '' ?>">
                                <div class="video-name"><?= $video->fields->title ?></div>
                                <div class="video-channel">В ногу со временем</div>
                                <div class="video-channel video-channel-statistic"></div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
 
        <div class="video-title hide-mob" id="title"><?= empty($videos[0]) ? '' : $videos[0]->fields->title ?></div>
        <div class="subtitle">В ногу со временем</div>
                <!-- <a href="https://www.youtube.com/user/STLNEWS770">
                <img class="follower" src="/wp-content/themes/twentythirteen/images/follow_youtube.png"/>
                </a> -->
                <div class="popular">
                    <header class="second-level-header"  >Популярное видео</header>
                    <div class="popular-list" id="slider"  >
                        <div class="popular-item">
                            <iframe src="https://www.youtube.com/embed/<?= empty($popular[0]) ? '' : $popular[0]->fields->id ?>?enablejsapi=1&amp;height=230"></iframe>
                        </div>
                        <div class="popular-item middle">
                            <iframe src="https://www.youtube.com/embed/<?= empty($popular[1]) ? '' : $popular[1]->fields->id ?>?enablejsapi=1&amp;height=230"></iframe>
                        </div>
                        <div class="popular-item">
                            <iframe src="https://www.youtube.com/embed/<?= empty($popular[2]) ? '' : $popular[2]->fields->id ?>?enablejsapi=1&amp;height=230"></iframe>
                        </div>
                    </div>
                </div>

    </div>
</div>
<script type="text/javascript">
    jQuery(function() {
        function onChangeSelection(){
            console.log('test');
            jQuery('.video-list-wrapper .video-wrapper').show();
            var glava = jQuery('.video-form select#video-glava').val();
            var dateh = jQuery('.video-form select#video-dateh').val();
            var dateg = jQuery('.video-form select#video-dateg').val();
            if(glava != '') {
                jQuery('.video-list-wrapper .video-wrapper[data-glava!="'+glava+'"]').hide();
            }
            if(dateh != '') {
                jQuery('.video-list-wrapper .video-wrapper[data-dateh!="'+dateh+'"]').hide();
            }
            if(dateg != '') {
                jQuery('.video-list-wrapper .video-wrapper[data-dateg!="'+dateg+'"]').hide();
            }
        }
        jQuery('.video-form select#video-glava').select2({
            placeholder: 'Выбрать главу',
            width: 222,
            allowClear: true
        }).on("change", onChangeSelection);
        jQuery('.video-form select#video-dateh').select2({
            placeholder: 'Выбрать дату',
            width: 300,
            allowClear: true
        }).on("change", onChangeSelection);
        jQuery('.video-form select#video-dateg').select2({
            placeholder: 'Выбрать дату',
            width: 300,
            allowClear: true
        }).on("change", onChangeSelection);
        var ids = [];
        jQuery('.video-content .controller > button').click(function(){
            if(jQuery(this).hasClass('prev')) {
                jQuery('.video-list-wrapper .video-wrapper.active').prev().click();
            }
            if(jQuery(this).hasClass('next')) {
                jQuery('.video-list-wrapper .video-wrapper.active').next().click();
            }
            if(jQuery(this).hasClass('update')) {
                window.location.reload();
            }
        });
        jQuery('.video-list-wrapper .video-wrapper').click(function(){
            jQuery('.video-container #player').html('<iframe src="https://www.youtube.com/embed/'+jQuery(this).data('id')+'?enablejsapi=1"></iframe>');
            jQuery('.video-list-wrapper .video-wrapper').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.video-title').html(jQuery(this).find('.video-name').text());
        });

        jQuery('.video-list-wrapper .video-wrapper').each(function(){
            ids.push(jQuery(this).data('id'))
        });
        ids = ids.join(',');
        jQuery.ajax({
            url: 'https://www.googleapis.com/youtube/v3/videos?part=statistics,snippet&id='+ids+'&key=AIzaSyCVI9ECxvHNb1l24_69GeJ2Er4xa_m1yJ4',
            contentType: 'application/json',
            dataType: 'json',
            success:function(data){
                for(key in data.items) {
                    var item = data.items[key];
                    var $video = jQuery('.video-list-wrapper .video-wrapper[data-id="'+item.id+'"]');
                    $video.find('.video-channel-statistic').html(item.statistics.viewCount+' просмотров');
                    // Если нужно брать название видео с ютуб
                    if($video.data('title') == '') {
                        $video.find('.video-name').html(item.snippet.title);
                    }

                    if(key == 0) {
                       jQuery('.video-title').html(item.snippet.title);
                    }
                }
            }
        });
    });
</script>

<style type="text/css" media="screen">
    html { margin-top: 0 !important; }
    * html body { margin-top: 0 !important; }
    @media screen and ( max-width: 782px ) {
        html { margin-top: 0 !important; }
        * html body { margin-top: 0 !important; }
    }
</style>
<?php require_once 'popup_donate.php'; ?>
<?php get_footer(); ?>
