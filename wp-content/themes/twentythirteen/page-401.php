<?php get_header(); ?>
<div class="announcements-page">
    <div class="container">
        <header class="header-wrapper">
            <div class="button-wrapper">
                <a href="/?page_id=728">
                    <span class="info-button">Архив</span>
                </a>
                <a href="<?php echo get_category_link(7); ?>"><span class="info-button">Календарь мероприятий</span></a>
                <a href="<?php echo get_permalink(401); ?>"><span class="info-button info-button__active">Анонсы</span>  </a>
            </div>
            <div class="header-title">
                    <a href="<?php echo get_category_link(2); ?>" class="news-link">
                    <i></i>
                     Новости
                     </a>
                    <div class="second-level-header">Анонсы</div>
            </div>
        </header>
    
        <?php 
          $my_query = new WP_Query(array( 'category' =>array(2,7), 'post_status'=>'future')); ?>
            <div class="future-posts-list">
                <?php  while ($my_query->have_posts()) : $my_query->the_post(); ?> 
                    <a href="<?php echo get_permalink();?>">
                        <div class="future-post-wrapper">
                            <div class="future-post-image"><?php the_post_thumbnail(); ?></div>
                            <div class="future-post-date"><?php the_date();?> </div>
                            <div class="future-post-title"><?php the_title();?></div>

                        </div>
                    </a>
               <?php endwhile; ?>
            </div>
            <?php get_sidebar(); ?>
     </div>
</div>
<?php get_footer(); ?>