<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="container splice-block-animation">
  <div class="main-page-conteiner splice-block-animation" >
    <div class="slider-wraper">
      <div class="comunity-card">
        <div class="name">Шиурей Тора Любавич </div>
        <div class="description">Еврейское молодежное движение Украины</div>
        <button class="get-cart get-button get-button_new">Получить карту участника</button>
      </div>
      <img class="shiurey-big" src="<?php echo get_template_directory_uri(); ?>/images/shiurey.png" />
      <div class="clear"></div>
      <div class="projects">
        <div class="second-level-header our-projects ">Наши проекты</div>
        <div class="stl-project project-item last-left" > 
          <img class="project-item-image" src="<?php echo get_template_directory_uri(); ?>/images/stl.png">
          <div class="describe">крупнейшее eврейское молодежное <br/>движение в Украине</div>
          <div class="go-to-project"><a href="<?php echo get_permalink(459) ?>" class="go-to-project">Перейти к проекту</a></div>

        </div>
        <div class="stars-project project-item middle-project">
          <img class="project-item-image" src="<?php echo get_template_directory_uri(); ?>/images/stars.png"/>
          <div class="describe">просветительный проект для молодежи 18-28 лет</div>
          <div class="go-to-project"><a href="<?php echo get_permalink(465) ?>" class="go-to-project">Перейти к проекту</a></div> 
        </div>
        <div class="university-project project-item middle-project"> 
          <img class="project-item-image" src="<?php echo get_template_directory_uri(); ?>/images/university.png"/>
          <div class="describe">совместный проект STL, "Бейт Хана" и Университета им. А.Нобеля</div>
          <div class="go-to-project"><a href="<?php echo get_permalink(463) ?>" class="go-to-project">Перейти к проекту</a></div> 
        </div>
        <div class="teens-project project-item middle-project"> 
          <img class="project-item-image" src="<?php echo get_template_directory_uri(); ?>/images/teens.png"/>
          <div class="describe">специальный проект для подростков <br/>12-17 лет</div>
          <div  class="go-to-project"><a href="<?php echo get_permalink(437) ?>" class="go-to-project">Перейти к проекту</a></div>    
        </div>
        <div class="light-project project-item last-right"> 
          <img class="project-item-image" src="<?php echo get_template_directory_uri(); ?>/images/light.png"/>
          <div class="describe">культурный проект <br/>для молодежи <br/>18-32 лет</div>
          <div class="go-to-project"><a href="<?php echo get_permalink(438) ?>" class="go-to-project">Перейти к проекту</a></div> 
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>  
</div>
    <article class="get-program-wrapper splice-block-animation">
      <div class="get-program-container"> 
        <div class="inner-container">
          <div class="second-level-header">Запишись на программу</div> 
          <div class="description"> Еврейское молодежное движение Украины</div>
        </div>
        <button onclick="popupShow('popup_signup')" class="get-button">Я хочу записаться сейчас!</button>
      </div>
      
    </article>
    <article class="news">
      <div class="container"> 
        <header class="news-header">
          <div class="button-wrapper">
            <!-- 
            <?php wp_get_archives('type=yearly'); ?>
             -->

            <a href="<?php echo get_category_link(7); ?>">
              <span class="info-button">Календарь мероприятий</span>
            </a>
            <a href="<?php echo get_permalink(401); ?>">
              <span class="info-button">Анонсы</span>
            </a>
          </div>
          <div class="title second-level-header">Наши новости</div>
        </header>
                <?php $news = get_posts(array(
                    'numberposts'     =>3, // тоже самое что posts_per_page
                    'offset'          => 0,
                    'category'        => '',
                    'orderby'         => 'post_date',
                    'order'           => 'DESC',
                    'include'         => '',
                    'exclude'         => '',
                    'meta_key'        => '',
                    'meta_value'      => '',
                    'post_type'       => 'post',
                    'post_mime_type'  => '', // image, video, video/mp4
                    'post_parent'     => '',
                    'post_status'     => 'publish',
                ));
                for($i=0; $i<3; $i++) {
                    if(empty($news[$i])) {
                        $news[$i] = new Wp_Post();
                    }
                }
                ?>
        <div class="news-grid animation-grid splice-block-animation" data-view-port-checker>
          <div class="big-section">
            <div class="events  hide-mob">
              <a href="<?php echo get_category_link(7); ?>" >
                <div class="check-event rounded-item news-block-main" data-view-port-checker-news="2">
                  <img class="mark" src="<?php echo get_template_directory_uri(); ?>/images/calendar.png" />
                  <div class="fourth-level-header">Выбрать ближайшее <br> мероприятие</div>
                </div>
              </a>
              <div class="publication news-block-main" style="width:509px;height:268px;background: url(<?php echo catch_that_image($news[1], get_template_directory_uri().'/images/vipusk.png') ?>);background-position: center; background-size: cover;"
                            data-view-port-checker-news="3">
                                <a href="<?php echo get_permalink($news[1]->ID); ?>" >
                    <!--img class="publication-image rounded-item" src="<?php echo catch_that_image($news[1], get_template_directory_uri().'/images/vipusk.png') ?>" /--> 
                    <div class="news-block-wrap">
                      
                    <div class="third-level-header">
                    <?= $news[1]->post_title ?>
                    </div>
                    <div class="date-time"><?php echo date('d.m.Y', strtotime($news[1]->post_date)); ?></div>
                      <div class="project-item-text"><?php echo $news[1]->post_content ?></div>
                    </div>
                    </a>
              </div>
            </div>
            <div class="study-program news-block-main" data-view-port-checker-news="1"
                        style="width:655px;height:373px;background: url(<?php echo catch_that_image($news[0], get_template_directory_uri().'/images/program.png') ?>);background-position: center; background-size: cover;"
                         >
                            <a href="<?php echo get_permalink($news[0]->ID); ?>">
                                <!--img class="study-image rounded-item" src="<?php echo catch_that_image($news[0], get_template_directory_uri().'/images/program.png') ?>" /-->
                                <div class="news-block-wrap">
                                <div class="third-level-header">
                                <?= $news[0]->post_title ?>
                                </div>
                                <div class="date-time"><?php echo date('d.m.Y', strtotime($news[0]->post_date)); ?></div>
                                <div class="project-item-text"><?php echo $news[0]->post_content ?></div>
                                </div>
                            </a>
             </div>

             <div class="events hide-screen">
               <a href="<?php echo get_category_link(7); ?>" >
                 <div class="check-event rounded-item news-block-main">
                   <img class="mark" src="<?php echo get_template_directory_uri(); ?>/images/calendar.png" />
                   <div class="fourth-level-header">Выбрать ближайшее <br> мероприятие</div>
                 </div>
               </a>
               <div class="publication news-block-main" style="width:509px;height:268px;background: url(<?php echo catch_that_image($news[1], get_template_directory_uri().'/images/vipusk.png') ?>);background-position: center; background-size: cover;"
                             data-view-port-checker-news="3">
                                 <a href="<?php echo get_permalink($news[1]->ID); ?>" >
                     <!--img class="publication-image rounded-item" src="<?php echo catch_that_image($news[1], get_template_directory_uri().'/images/vipusk.png') ?>" /--> 
                     <div class="news-block-wrap">
                       
                     <div class="third-level-header">
                        <?= $news[1]->post_title ?>
                     </div>
                     <div class="date-time"><?php echo date('d.m.Y', strtotime($news[1]->post_date)); ?></div>
                      <div class="project-item-text"><?= $news[1]->post_content ?></div>
                     </div>
                     </a>
               </div>
             </div>
          </div>
          <div class="small-section"> 
                        <a href="#" onclick="popupShow('popup_donate'); return false">
            <div class="support small-item news-block-main" data-view-port-checker-news="4">
              <div class="fourth-level-header"> Поддержать<br> проект</div>
              <img src="<?php echo get_template_directory_uri(); ?>/images/time.png" />
            </div>
                        </a>
            <a href="<?php echo get_permalink(401); ?>" >
            <div class="learn small-item news-block-main" data-view-port-checker-news="5">
              <div class="fourth-level-header">Выучить<br> сегодня </div>
              <img src="<?php echo get_template_directory_uri(); ?>/images/learn.png" /> 
            </div>
                        </a>
            <div class="seminar news-block-main"
                        style="width:415px;height:170px;background: url(<?php echo catch_that_image($news[2], get_template_directory_uri().'/images/seminar.png') ?>);background-position: center; background-size: cover;"
                        data-view-port-checker-news="6">
                            <a href="<?php echo get_permalink($news[2]->ID); ?>">
                                <!--img class="seminar-image" src="<?php echo catch_that_image($news[2], get_template_directory_uri().'/images/seminar.png') ?>" /--> 
                                <div class="news-block-wrap">
                                  <div class="third-level-header">
                                    <?= $news[2]->post_title ?>
                                  </div>
                                  <div class="date-time"><?php echo date('d.m.Y', strtotime($news[2]->post_date)); ?></div>
                                  <div class="project-item-text"><?php echo $news[2]->post_content ?></div>
                                </div>
                            </a>
            </div>
            <a href="#" onclick="popupShow('popup_signup'); return false" >
              <div class="join small-item news-block-main" data-view-port-checker-news="7">
                <div class="fourth-level-header"> Записаться на<br> программу</div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/join.png" /> 
              </div>
            </a>
          
          </div> 
          <div class="clear"></div>
          <div  class="more" >
                        <a href="<?php echo get_category_link(2); ?>"><button class="info-button all_news">Больше новостей</button></a>
          </div>
          <div class="clear"></div>
        
        </div>
      </div>
    </article>
    <article class="stl-channel splice-block-animation"> 
      <div class="container"> 
        <div class="title"  data-view-port-checker-right> 
          <div class="second-level-header">STL channel</div>
          <div class="follow">Подпишись на наш канал в </div>
          <div class="youtube">Youtube</div>
        </div>
        <div class="youtube_main" data-view-port-checker-right>
          <a href="https://www.youtube.com/user/STLNEWS770">
              <img class="follower" src="/wp-content/themes/twentythirteen//images/follow_youtube.png"/>
          </a>
        </div>

        <div class="video-wrapper" data-scroll-hide>
          <!--
          <?php /* The loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>
          <div  >
            <?php remove_filter( 'the_content', 'wpautop' ); ?>
          </div>
          <div  class="video-content-main">
           <?php the_content(); ?>
          </div>
        <?php endwhile; ?>

          -->
          </div>
        
    </article>
    <div class="clear"></div>
    <article class="com-card">
      <div class="container">
      <div class="title-cp">
      <div class="second-level-header">Community-card</div>
      </div>
      <div class="com-feedback"> 
        <?= do_shortcode('[contact-form-7 id="571" title="Контактная форма"] '); ?>
     </div>
      <img class="jew" src="/wp-content/themes/twentythirteen/images/jev_1.png" alt="" />

      </div>
    </article>



<?php require_once 'popup_donate.php'; ?>
<?php require_once 'popup_signup.php'; ?>
<?php get_footer(); ?>

