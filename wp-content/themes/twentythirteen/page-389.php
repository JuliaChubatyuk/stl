<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="container">
    <div class="calendar-page-wrapper">

        <header class="header-wrapper">
            <div class="button-wrapper">
                <a href="/?page_id=728">
                    <span class="info-button">Архив</span>
                </a>
                <a href="<?php echo get_category_link(7); ?>"><span class="info-button">Календарь мероприятий</span></a>
                <a href="<?php echo get_permalink(401); ?>"><span class="info-button">Анонсы</span></a>
            </div>
            <div class="header-title">
                <a href="<?php echo get_category_link(2); ?>" class="news-link">
                    <i></i>
                    Новости
                </a>
                <div class="second-level-header">Календарь мероприятий</div>
            </div>
        </header>
       <div class="news-article">
         <?php  $count = wp_count_posts()->publish; //number of our posts
                $count2 = wp_count_posts()->future;
                $count +=count2;
                $date = strtotime($_GET['date']); // sort date
            query_posts('posts_per_page='.$count); 
            if (have_posts()) : 
                while (have_posts()) : the_post(); 
                        //sort by date and category 'news' 
                    if(in_category(2,get_the_ID()) & strtotime(get_the_date()) == $date){ ?> 
                        
                        <a href="<?php the_permalink() ?>" class="min-post-container">
                            <div class="min-post-photo"><?php the_post_thumbnail( array(700, 350) );?></div>
                            <div class="min-post-date"><?php the_date(); ?></div>
                            <div class="min-post-title"><?php the_title(); ?></div>
                        </a>
                        
                    <?php } ?>

               <?php endwhile; ?>
            <?php endif; ?>
           
        </div>
        
       
        <?php get_sidebar();?>
        <?php dynamic_sidebar('sidebar-2')?>
       <div class="clear"></div>
     
    </div>
</div>
<?php get_footer();?>
