<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div  style="width: 1229px; margin: 0 auto">
    <header>
        <div class="header">
            <div class="second-level-header">Shiurey Torah Lubavich</div>
            <button class="pp" onclick="popupShow('popup_donate')">Поддержать проект</button>
        </div>
    </header>
    <div class="clear"></div><!-- clear -->
    <main>
        <div class="main main-project">
            <img class="pr-stl-img_0"
              src="/wp-content/themes/twentythirteen/images/pr-torah-logo.png" alt="pr-stl-logo"/>
            <div class="pr-stl_text_0 pr-stl_text_1_border" >Shiurey Torah Lubavich (Шиурéй Торá Любáвич, STL) – благотворительный фонд, основным направлением деятельности которого является еврейское просвещение в Украине и по всему миру.</div>
            <!-- <a href="#"><div class="pr-stl_arrow_left"></div></a>
             <a href="#"><div class="pr-stl_arrow_right"></div></a>-->
            <img  src="/wp-content/themes/twentythirteen/images/foto7.jpg" alt="foto7" class="pr-stl-img_2 z2"/>
            <div 

            class="pr-stl_texttorah_2 "><span>Фонд основан</span><br/>
                23 января 2002 года (10 швата 5762 года по еврейскому календарю). Благословение на создание организации дал лично главный раввин Днепропетровска Шмуэль Каминецкий
            </div>
            <div class="pr-stl-predstavit animateme scrollme"
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-translatey="180" >
                <div><span>Представительства</span></div>
                <div class="face_1">в Днепропетровске<br/> <strong>Элиша Гавриэль Павлоцкий</strong><br/>руководитель</div>
                <div class="face_2">в Киеве<br/> <strong>Арье Рафаэль Низевич</strong><br/>руководитель</div>
            </div>
            <div  class="pr-torah-text_3 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-opacity="0"
            data-translatey="300"><span>Миссия фонда</span>
                <br/>
                <div class="pr-torah-text_3_border">пропаганда еврейского образа жизни, через соблюдение заповедей и изучение Торы.</div>
                <span>Ключевая цель</span><br/>
                стать крупнейшим еврейским молодежным движением в мире.
            </div>
            <div class="clear"></div><!-- clear -->
            <div  class="pr-torah-punkti animateme scrollme"
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-opacity="0"
            data-translatey="100">
                <span>Направления</span>
                <ul>
                    <li>распространение традиционной еврейской культуры
                        (изучение традиций);</li>
                    <li>молодежные проекты;</li>
                    <li>специализированные медиа-проекты;</li>
                    <li>организация мероприятий;</li>
                    <li>программы для женщин.</li>
                </ul>
            </div>

            <img  src="/wp-content/themes/twentythirteen/images/foto8.jpg" alt="foto7" class="pr-stl-img_7 z0 animateme scrollme"/
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-translatey="400">

            <img  src="/wp-content/themes/twentythirteen/images/foto9.jpg" alt="foto9" class="pr-stl-img_0 z1 marL28 animateme scrollme"/
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-translatey="100">

            <div  class="pr-torah-text_4 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-opacity="0"
            data-translatey="400">Уже более 1000 человек стали студентами наших программ, посвященных изучению еврейских традиций. Каждое молодежное мероприятие посещает от 100 до 300 человек.</div>

            <div  class="pr-torah-text_5 animateme scrollme"
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-opacity="0"
            data-translatey="100">Высшая радость для всех, кто связан с Шиурей Тора Любавич, - это, конечно же, когда проходят еврейские свадьбы, обрезания, опшерниши, бар и бат мицвы. И только за последний год среди участников наших проектов образовалось больше 15 новый еврейских семей!</div>

            <img  src="/wp-content/themes/twentythirteen/images/foto11.jpg" alt="foto11" class="pr-stl-img_8 animateme scrollme"
            data-when="view"
            data-from=".5"
            data-to=".1"
            data-translatey="300"/>

            <img  src="/wp-content/themes/twentythirteen/images/foto10.jpg" alt="foto10" class="pr-stl-img_9 animateme scrollme" 
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-translatey="150"/>

            <div  class="pr-torah-text_6 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-opacity="0"
            data-translatey="450">Каждый год делегация Шиурей Тора Любавич ездит к Ребе в Нью Йорк, чтобы получить его благословение и рассказать о наших успехах, а также тех людях, которые нам помогали.</div>

            <div class="clear"></div><!-- clear -->
            <div class="second-level-header">Другие проекты</div>
        </div>
    </main>
    <div class="clear"></div><!-- clear -->
</div>
  <footer class="footer">

        <div class="pr-stl">
            <div class="pr-stl_wrapper">

                <div class="floatL">
                    <a href="<?php echo get_permalink(465); ?>"><div class="pr-stl_content pr-stl_content_2"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(463); ?>"><div class="pr-stl_content pr-stl_content_3"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(437); ?>"><div class="pr-stl_content pr-stl_content_4"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(438); ?>"><div class="pr-stl_content pr-stl_content_5"></div></a>
                </div>
            </div>
        </div>
    </footer>
<?php require_once 'popup_donate.php'; ?>

<script >
    $(document).ready(function(){
        if(window.innerWidth > 500){
         var script_scroll = $("<script>");
         script_scroll.attr("src", '/wp-content/themes/twentythirteen/js/jquery.scrollme.min.js')
         $(document.body).append(script_scroll);
        }
    });
</script>

<?php get_footer(); ?>