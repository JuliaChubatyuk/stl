<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta name="google-site-verification" content="W1Dbt-wTB7NwIOCPQjUgwBkmjnoKncGTZ65O8gBrHq4" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?= esc_url( get_template_directory_uri() ); ?>/css/stl.css" />
	
	<link rel="stylesheet" type="text/css" href="<?= esc_url( get_template_directory_uri() ); ?>/css/animate.css" />

	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Salsa' rel='stylesheet' type='text/css'>
    <link href="/wp-content/themes/twentythirteen/css/social-likes_classic.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/social-likes.min.js"></script>
    <script type="text/javascript" src="/wp-content/themes/twentythirteen/js/jquery.dotdotdot.min.js"></script>

    <script src="https://cdn.jsdelivr.net/anythingslider/1.8.6/jquery.anythingslider.js"></script>
    <script src="/wp-content/themes/twentythirteen/js/jquery.slides.min.js"></script>
    <script src="/wp-content/themes/twentythirteen/js/jquery.viewportchecker.min.js"></script>
	<?php wp_head(); ?>
</head>

<script type="text/javascript">
	$(document).ready(function(){
		$('body').on('click', '#primary-menu a', function(e) {

			if($(this).parent().find('.sub-menu').length && $(this).parent().find('.sub-menu').attr('state') != 'true') {
				e.preventDefault();
				$(this).parent().find('.sub-menu').attr('state', 'true');
				return;
			}
		})
	});
</script>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed1 site1">
		<header class="stl">
			<div class="container">
				<div class="logo-describe">Лидер неформального еврейского <div class="item">просвещения</div></div>
				<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<img class="logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/stl-logo.png"/>
				</a>
			</div>
			
		
		<div class="navigation-wrapper"> 
				<nav class="navigation-main">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
				</nav>
			</div>	
        </header>
		<div id="main" class="site-main <?php if (is_front_page()) { echo 'main-page'; } ?>">
