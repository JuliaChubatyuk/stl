<?php get_header(); ?>
    
    <div class="container">
        <div class="calendar-events-page">
            <header class="header-wrapper">
                <div class="button-wrapper">

                    <a href="/?page_id=728">
                        <span class="info-button">Архив</span>
                    </a>

                    <a href="<?php echo get_category_link(7); ?>"><span class="info-button info-button__active">Календарь мероприятий</span></a>
                    <a href="<?php echo get_permalink(401); ?>"><span class="info-button">Анонсы</span>  </a>
                </div>
                <div class="header-title">
                    <a href="<?php echo get_category_link(2); ?>" class="news-link">
                    <i></i>
                    Новости
                    </a>
                    <div class="second-level-header">Календарь мероприятий</div>
                </div>
            </header>
            <div class="event-page">
                <div class="events-list">
                <?php /* The loop */ 
                $count = wp_count_posts()->publish; //number of our posts
                query_posts('posts_per_page='.$count);
                    while (have_posts()) : the_post(); 
                            //sort by  category 'news' 
                        if(in_category(7,get_the_ID())){ ?>
                            <a href="<?php echo get_permalink();?>">
                                <div class="event-wrapper">
                                    <div class="event-image"><?php the_post_thumbnail(); ?></div>
                                    <div class="event-date"><?php the_date();?></div>
                                    <div class="event-title"><?php the_title(); ?></div> 
                                </div>   
                            </a>
                        <?php } ?>
                    <?php endwhile; ?>
                </div>
                <?php get_sidebar();?>
                <div class="clear"></div> 
            </div>
        </div>
        </div>
<?php get_footer(); ?>
