<?php
/**
 * Twenty Thirteen functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see https://codex.wordpress.org/Theme_Development
 * and https://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link https://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

/*
 * Set up the content width value based on the theme's design.
 *
 * @see twentythirteen_content_width() for template-specific adjustments.
 */
if ( ! isset( $content_width ) )
  $content_width = 604;

/**
 * Add support for a custom header image.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Twenty Thirteen only works in WordPress 3.6 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6-alpha', '<' ) )
  require get_template_directory() . '/inc/back-compat.php';

/**
 * Twenty Thirteen setup.
 *
 * Sets up theme defaults and registers the various WordPress features that
 * Twenty Thirteen supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_setup() {
  /*
   * Makes Twenty Thirteen available for translation.
   *
   * Translations can be added to the /languages/ directory.
   * If you're building a theme based on Twenty Thirteen, use a find and
   * replace to change 'twentythirteen' to the name of your theme in all
   * template files.
   */
  load_theme_textdomain( 'twentythirteen', get_template_directory() . '/languages' );

  /*
   * This theme styles the visual editor to resemble the theme style,
   * specifically font, colors, icons, and column width.
   */
  add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', twentythirteen_fonts_url() ) );

  // Adds RSS feed links to <head> for posts and comments.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Switches default core markup for search form, comment form,
   * and comments to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
  ) );

  /*
   * This theme supports all available post formats by default.
   * See https://codex.wordpress.org/Post_Formats
   */
  add_theme_support( 'post-formats', array(
    'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
  ) );

  // This theme uses wp_nav_menu() in one location.
  register_nav_menu( 'primary', __( 'Navigation Menu', 'twentythirteen' ) );

  /*
   * This theme uses a custom image size for featured images, displayed on
   * "standard" posts and pages.
   */
  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 235, 160, true );

  // This theme uses its own gallery styles.
  add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'twentythirteen_setup' );

/**
 * Return the Google font stylesheet URL, if available.
 *
 * The use of Source Sans Pro and Bitter by default is localized. For languages
 * that use characters not supported by the font, the font can be disabled.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string Font stylesheet or empty string if disabled.
 */
function twentythirteen_fonts_url() {
  $fonts_url = '';

  /* Translators: If there are characters in your language that are not
   * supported by Source Sans Pro, translate this to 'off'. Do not translate
   * into your own language.
   */
  $source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'twentythirteen' );

  /* Translators: If there are characters in your language that are not
   * supported by Bitter, translate this to 'off'. Do not translate into your
   * own language.
   */
  $bitter = _x( 'on', 'Bitter font: on or off', 'twentythirteen' );

  if ( 'off' !== $source_sans_pro || 'off' !== $bitter ) {
    $font_families = array();

    if ( 'off' !== $source_sans_pro )
      $font_families[] = 'Source Sans Pro:300,400,700,300italic,400italic,700italic';

    if ( 'off' !== $bitter )
      $font_families[] = 'Bitter:400,700';

    $query_args = array(
      'family' => urlencode( implode( '|', $font_families ) ),
      'subset' => urlencode( 'latin,latin-ext' ),
    );
    $fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
  }

  return $fonts_url;
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_scripts_styles() {
  /*
   * Adds JavaScript to pages with the comment form to support
   * sites with threaded comments (when in use).
   */
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
    wp_enqueue_script( 'comment-reply' );

  // Adds Masonry to handle vertical alignment of footer widgets.
  if ( is_active_sidebar( 'sidebar-1' ) )
    wp_enqueue_script( 'jquery-masonry' );

  // Loads JavaScript file with functionality specific to Twenty Thirteen.
  wp_enqueue_script( 'twentythirteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );

  // Add Source Sans Pro and Bitter fonts, used in the main stylesheet.
  wp_enqueue_style( 'twentythirteen-fonts', twentythirteen_fonts_url(), array(), null );

  // Add Genericons font, used in the main stylesheet.
  wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.03' );

  // Loads our main stylesheet.
  wp_enqueue_style( 'twentythirteen-style','/wp-content/themes/twentythirteen/style.css?ver=4.2.2' ); //get_stylesheet_uri(), array(), '2013-07-18'  

  // Loads the Internet Explorer specific stylesheet.
  wp_enqueue_style( 'twentythirteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentythirteen-style' ), '2013-07-18' );
  wp_style_add_data( 'twentythirteen-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'twentythirteen_scripts_styles' );

/**
 * Filter the page title.
 *
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */
function twentythirteen_wp_title( $title, $sep ) {
  global $paged, $page;

  if ( is_feed() )
    return $title;

  // Add the site name.
  $title .= get_bloginfo( 'name', 'display' );

  // Add the site description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    $title = "$title $sep $site_description";

  // Add a page number if necessary.
  if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
    $title = "$title $sep " . sprintf( __( 'Page %s', 'twentythirteen' ), max( $paged, $page ) );

  return $title;
}
add_filter( 'wp_title', 'twentythirteen_wp_title', 10, 2 );

/**
 * Register two widget areas.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Main Widget Area', 'twentythirteen' ),
    'id'            => 'sidebar-1',
    'description'   => __( 'Appears in the footer section of the site.', 'twentythirteen' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Secondary Widget Area', 'twentythirteen' ),
    'id'            => 'sidebar-2',
    /*'description'   => __( 'Appears on posts and pages in the sidebar.', 'twentythirteen' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',*/
  ) );
    
    register_sidebar(array(
        'name'          => __('Third Widget Area', 'twentythirteen'),
        'id'            => 'sidebar-3',
    ));
}
add_action( 'widgets_init', 'twentythirteen_widgets_init' );

if ( ! function_exists( 'twentythirteen_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_paging_nav() {
  global $wp_query;

  // Don't print empty markup if there's only one page.
  if ( $wp_query->max_num_pages < 2 )
    return;
  ?>
  <nav class="navigation paging-navigation" role="navigation">
    <h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'twentythirteen' ); ?></h1>
    <div class="nav-links">

      <?php if ( get_next_posts_link() ) : ?>
      <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> предыдущая', 'twentythirteen' ) ); ?></div>
      <?php endif; ?>

      <?php if ( get_previous_posts_link() ) : ?>
      <div class="nav-next"><?php previous_posts_link( __( '<span class="meta-nav">&rarr;</span> следующая', 'twentythirteen' ) ); ?></div>
      <?php endif; ?>

    </div><!-- .nav-links -->
  </nav><!-- .navigation -->
  <?php
}
endif;

if ( ! function_exists( 'twentythirteen_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
*
* @since Twenty Thirteen 1.0
*/
function twentythirteen_post_nav() {
  global $post;

  // Don't print empty markup if there's nowhere to navigate.

  $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post_by_tag( false, '', true );
  $next     = get_fixed_adjacent_post( false, '', false );

  if ( ! $next && ! $previous )
    return;
  ?>
  <nav class="navigation post-navigation" role="navigation">
    <h1 class="screen-reader-text"><?php _e( 'Post navigation', 'twentythirteen' ); ?></h1>
    <div class="nav-links">

      <?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> предыдущая', 'Previous post link', 'twentythirteen' ) ); ?>
      <?php next_post_link( '%link', _x( 'слудующая <span class="meta-nav">&rarr;</span>', 'Next post link', 'twentythirteen' ) ); ?>

    </div><!-- .nav-links -->
  </nav><!-- .navigation -->
  <?php
}
endif;

if ( ! function_exists( 'twentythirteen_entry_meta' ) ) :
/**
 * Print HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own twentythirteen_entry_meta() to override in a child theme.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_entry_meta() {
  if ( is_sticky() && is_home() && ! is_paged() )
    echo '<span class="featured-post">' . esc_html__( 'Sticky', 'twentythirteen' ) . '</span>';

  if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
    twentythirteen_entry_date();

  // Translators: used between list items, there is a space after the comma.
  $categories_list = get_the_category_list( __( ', ', 'twentythirteen' ) );
  if ( $categories_list ) {
    echo '<span class="categories-links">' . $categories_list . '</span>';
  }

  // Translators: used between list items, there is a space after the comma.
  $tag_list = get_the_tag_list( '', __( ', ', 'twentythirteen' ) );
  if ( $tag_list ) {
    echo '<span class="tags-links">' . $tag_list . '</span>';
  }

  // Post author
  if ( 'post' == get_post_type() ) {
    printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
      esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
      esc_attr( sprintf( __( 'View all posts by %s', 'twentythirteen' ), get_the_author() ) ),
      get_the_author()
    );
  }
}
endif;

if ( ! function_exists( 'twentythirteen_entry_date' ) ) :
/**
 * Print HTML with date information for current post.
 *
 * Create your own twentythirteen_entry_date() to override in a child theme.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param boolean $echo (optional) Whether to echo the date. Default true.
 * @return string The HTML-formatted post date.
 */
function twentythirteen_entry_date( $echo = true ) {
  if ( has_post_format( array( 'chat', 'status' ) ) )
    $format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'twentythirteen' );
  else
    $format_prefix = '%2$s';

  $date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
    esc_url( get_permalink() ),
    esc_attr( sprintf( __( 'Permalink to %s', 'twentythirteen' ), the_title_attribute( 'echo=0' ) ) ),
    esc_attr( get_the_date( 'c' ) ),
    esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
  );

  if ( $echo )
    echo $date;

  return $date;
}
endif;

if ( ! function_exists( 'twentythirteen_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_the_attached_image() {
  /**
   * Filter the image attachment size to use.
   *
   * @since Twenty thirteen 1.0
   *
   * @param array $size {
   *     @type int The attachment height in pixels.
   *     @type int The attachment width in pixels.
   * }
   */
  $attachment_size     = apply_filters( 'twentythirteen_attachment_size', array( 724, 724 ) );
  $next_attachment_url = wp_get_attachment_url();
  $post                = get_post();

  /*
   * Grab the IDs of all the image attachments in a gallery so we can get the URL
   * of the next adjacent image in a gallery, or the first image (if we're
   * looking at the last image in a gallery), or, in a gallery of one, just the
   * link to that image file.
   */
  $attachment_ids = get_posts( array(
    'post_parent'    => $post->post_parent,
    'fields'         => 'ids',
    'numberposts'    => -1,
    'post_status'    => 'inherit',
    'post_type'      => 'attachment',
    'post_mime_type' => 'image',
    'order'          => 'ASC',
    'orderby'        => 'menu_order ID',
  ) );

  // If there is more than 1 attachment in a gallery...
  if ( count( $attachment_ids ) > 1 ) {
    foreach ( $attachment_ids as $attachment_id ) {
      if ( $attachment_id == $post->ID ) {
        $next_id = current( $attachment_ids );
        break;
      }
    }

    // get the URL of the next image attachment...
    if ( $next_id )
      $next_attachment_url = get_attachment_link( $next_id );

    // or get the URL of the first image attachment.
    else
      $next_attachment_url = get_attachment_link( reset( $attachment_ids ) );
  }

  printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
    esc_url( $next_attachment_url ),
    the_title_attribute( array( 'echo' => false ) ),
    wp_get_attachment_image( $post->ID, $attachment_size )
  );
}
endif;

/**
 * Return the post URL.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string The Link format URL.
 */
function twentythirteen_get_link_url() {
  $content = get_the_content();
  $has_url = get_url_in_content( $content );

  return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

if ( ! function_exists( 'twentythirteen_excerpt_more' ) && ! is_admin() ) :
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ...
 * and a Continue reading link.
 *
 * @since Twenty Thirteen 1.4
 *
 * @param string $more Default Read More excerpt link.
 * @return string Filtered Read More excerpt link.
 */
function twentythirteen_excerpt_more( $more ) {
  $link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
    esc_url( get_permalink( get_the_ID() ) ),
      /* translators: %s: Name of current post */
      sprintf( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'twentythirteen' ), '<span class="screen-reader-text">' . get_the_title( get_the_ID() ) . '</span>' )
    );
  return '...';
}
add_filter( 'excerpt_more', 'twentythirteen_excerpt_more' );
endif;

function custom_excerpt_length( $length ) {
  return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function twentythirteen_body_class( $classes ) {
  if ( ! is_multi_author() )
    $classes[] = 'single-author';

  if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
    $classes[] = 'sidebar';

  if ( ! get_option( 'show_avatars' ) )
    $classes[] = 'no-avatars';

  return $classes;
}
add_filter( 'body_class', 'twentythirteen_body_class' );

/**
 * Adjust content_width value for video post formats and attachment templates.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_content_width() {
  global $content_width;

  if ( is_attachment() )
    $content_width = 724;
  elseif ( has_post_format( 'audio' ) )
    $content_width = 484;
}
add_action( 'template_redirect', 'twentythirteen_content_width' );

/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function twentythirteen_customize_register( $wp_customize ) {
  $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
  $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
  $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'twentythirteen_customize_register' );

/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JavaScript handlers to make the Customizer preview
 * reload changes asynchronously.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_customize_preview_js() {
  wp_enqueue_script( 'twentythirteen-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20141120', true );
}
add_action( 'customize_preview_init', 'twentythirteen_customize_preview_js' );

add_image_size('about',344,335,false);

function get_more_excerpt($post){

  $p1 = get_extended($post->post_content);
  return $p1["main"];
}

/*display date and*/

 function recentPostsDate() {
    $rPosts = $my_query = new WP_Query('cat=2&showposts=3&orderby=rand');

    while ($rPosts->have_posts()) : $rPosts->the_post(); ?>
    <!--Check category id -->
            <div class="item">
                <a href="<?php the_permalink(); ?>">
                    <?php   the_date('d.m.Y', '<div class="date">', '</div>'); 
                            the_title(); ?>
                </a>
            </div>
    <?php endwhile;
    wp_reset_query(); 
}


function getNextPostsDate(){
        
        $my_query = new WP_Query('cat=2&showposts=3&orderby=rand');
        

        while ($my_query->have_posts()) : $my_query->the_post();


        echo '<div class="next-post-wrapper"> 
                <div class="next-post-text">
                    <div class="next-post-date">' .get_the_date().'</div>
                    <a class="next-post-title" href='.get_the_permalink().'>'. get_the_title() .'</a>'.
                '</div>'.
                '<div class="next-post-image">'.get_the_post_thumbnail()  . '</div>
            </div>' ; 

        endwhile;
        $post = $current_post; 
}



// запрет обновления выборочных плагинов
function filter_plugin_updates( $update ) {    
    global $DISABLE_UPDATE; // см. wp-config.php
    if( !is_array($DISABLE_UPDATE) || count($DISABLE_UPDATE) == 0 ){  return $update;  }
 
    if(!empty($update->response))
    foreach( $update->response as $name => $val ){
        foreach( $DISABLE_UPDATE as $plugin ){
            if( stripos($name,$plugin) !== false ){
                unset( $update->response[ $name ] );
            }
        }
    }
    return $update;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );


function numeric_posts_nav($query = null) {
  if( is_singular() && empty($query) )
    return;

  global $wp_query;
  if(empty($query)) {
    $query = $wp_query;
  }

  /** Stop execution if there's only 1 page */
  if( $query->max_num_pages <= 1 )
    return;

  $page = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
  $maxpage   = intval($query->max_num_pages);
  ?>
    <div class="post-paging">
        <nav class="navigation" role="navigation">
            <div class="nav-links">
                <?php if($page > 1) :?>
                    <a class="nav-prev" href="<?= get_pagenum_link($page - 1) ?>">&lt;</a>
                <?php else : ?>
                    <span class="nav-prev" >&lt;</span>
                <?php endif ?>
                <?php for($p = 1;$p <= $maxpage; $p++) : ?>
                    <?php if($p < 3
                        OR $p > $maxpage - 2
                        OR ($p > $page - 2 AND $p < $page + 2)
                    ) :
                        $dots = true;
                    ?>
                        <?php if($page <> $p) : ?>
                            <a class="nav-link" href="<?= get_pagenum_link($p) ?>"><?= $p ?></a>
                        <?php else : ?>
                            <span class="nav-current" ><?= $p ?></span>
                        <?php endif ?>
                    <?php else : 
                        if($dots) {
                            echo '...';
                        }
                        $dots = false;
                        endif ?>
                <?php endfor ?>
                <?php if($page < $maxpage) : ?>
                    <a class="nav-next" href="<?= get_pagenum_link($page + 1) ?>">&gt;</a>
                <?php else : ?>
                    <span class="nav-next" >&gt;</span>
                <?php endif ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
    </div>
  <?php

}

function custom_posts_per_page($query){
    if($query->get( 'cat' ) == 2){
        $query->set('posts_per_page', 13);
    }
    elseif(is_archive()){
        $query->set('posts_per_page',8);
  }//endif
}//function

//this adds the function above to the 'pre_get_posts' action
add_action('pre_get_posts','custom_posts_per_page');

function catch_that_image($post, $default) {
  $text = $post->post_content;
  $thumbnail = get_the_post_thumbnail($post->ID);
  $post_thumbnail_id = get_post_thumbnail_id($post->ID);
  if(!empty($post_thumbnail_id)) {
    $thumbnail = wp_get_attachment_image_src($post_thumbnail_id, 'large' );
    return $thumbnail[0];
  }

  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $text, $matches);
  $first_img = $matches[1][0];
  $first_img = preg_replace('|https?://[^/]+|is', '', $first_img);
  if(empty($first_img) || !file_exists('.'.$first_img)) {
    return $default;
  }
  return $first_img;
}

function youtube_attachments( $attachments )
{

$fields         = array(
  array(
    'name'      => 'title',
    'type'      => 'text',
    'label'     => 'Название',
    'default'   => 'title',
  ),
  array(
    'name'      => 'link',
    'type'      => 'text',
    'label'     => 'Ссылка',
  ),
  array(
    'name'      => 'glava',
    'type'      => 'text',
    'label'     => 'Глава',
  ),
  array(
    'name'      => 'dateh',
    'type'      => 'date',
    'label'     => 'время в Еврейском календаре',
    'meta' => [
        'data' => [
            'calendar' => 'hebrew',
        ],
    ],
  ),
  array(
    'name'      => 'dateg',
    'type'      => 'date',
    'label'     => 'время в Григорианском календаре',
  ),
);

  $args = array(
    'label'         => 'Youtube видео',
    'post_type'     => array( 'post', 'page' ),
    'position'      => 'normal',
    'priority'      => 'high',
    'filetype'      => null,  // no filetype limit
    'note'          => 'Добавьте видео с Youtube',
    'append'        => 'no',
    'nofile'        => true,
    'button_text'   => __('Добавьте видео', 'attachments'),
    'modal_text'    => __('Добавить', 'attachments'),
    'router'        => 'browse',
    'post_parent'   => false,
    'fields'        => $fields,
  );

  $attachments->register('youtube_attachments', $args); // unique instance name
}

add_action('attachments_register', 'youtube_attachments');


function catch_excerpt($text, $len = 200) {
  return preg_replace('|^(.{'.$len.'}).*|isu', '$1 ...', strip_tags($text));
}

add_filter('get_archives_link', 'get_archive_links_css_class' );
function get_archive_links_css_class($link) {
    return str_replace('Архив', '<span class="info-button">Архив</span>', $link);
}


add_filter('the_posts', 'show_all_future_posts');

function show_all_future_posts($posts) {
    global $wp_query, $wpdb;

    if(is_single() && $wp_query->post_count == 0) {
        $posts = $wpdb->get_results($wp_query->request);
    }

    return $posts;
}
