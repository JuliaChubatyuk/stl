<?php get_header(); ?>
<div class="container">
    <div class="contacts-page">
     <header class="header-wrapper">
            <div class="header-title">Контакты</div>
        </header>
        <div class="contacts-content">
            <div class="sidebar">
                <div class="agency">
                    <div id="kiev" class="header">Киевское <br />предствительство</div>
                    <div class="agency-title">Адрес:</div>
                    <div class="agency-text">ул Шота Руставели, 13<br>синагога Бродского</div> 
                    
                    <div class="agency-title">Телефоны:</div>
                    <div class="agency-text">067 567 77 42<br />044 235 90 82</div>  
                    
                    <div class="agency-title">Написать нам:</div>
                    <div class="agency-email">shitokiev@gmail.com</div>
                </div>
                <div class="agency">
                    <div id="dnepr" class="header">Днепропетровское <br />предствительство</div>
                    <div class="agency-title">Адрес:</div>
                    <div class="agency-text">ул Шолом-Алейхема, 4/26<br>центр &#171 Менора &#187</div> 
                    
                    <div class="agency-title">Телефоны:</div>
                    <div class="agency-text"> 067 525 27 70<br />050 424 27 70<br />093 307 37 70</div>  
                    
                    <div class="agency-title">Написать нам:</div>
                    <div class="agency-email">shitokiev@gmail.com</div>
                </div>
            </div>
            <div class="map-wrapper">
                <div id="map" style="border-radius: 5px; box-shadow: 1px 2px 2px #ccc;"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz3qAuBZIgkfGeFrkMURUceID8YdeYatA"></script>
<script src="wp-content/themes/twentythirteen/js/map-controller.js"></script>
<?php get_footer(); ?>