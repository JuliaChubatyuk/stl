<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="container">
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<div class="news-page-header">
                <div class="button-wrapper">
                    

                    <a href="/?page_id=728">
                        <span class="info-button">Архив</span>
                    </a>
                    <a href="<?php echo get_category_link(7); ?>">
                        <span class="info-button">Календарь мероприятий</span>
                    </a>
                    <a href="<?php echo get_permalink(401); ?>"><span class="info-button">Анонсы</span></a>
                </div>
                <a href="<?php echo get_category_link(2); ?>" class="news-link">
                    <i></i> 
                    Новости
                </a>
                <div class="clear"></div>
			</div>

			<div class="news-page">
            <!--content -->
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php if(!in_category(4,get_the_ID())){ ?>        
				<?php } ?>
				<?php //comments_template(); ?>
            <!-- #content -->
			<?php endwhile; ?>

                <?php get_sidebar(); ?>
                
                 <div class="clear"></div>
                <?php if(in_category(2,get_the_ID())){ ?>    
                    <ul class="social-likes" >
                        <li class="facebook custom-facebook" data-url="https://www.facebook.com/stlubavich" title="Like"></li>
                        <li class="vkontakte custom-vkontakte"></li>
                        <li class="plusone custom-plusone" data-url="https://plus.google.com/u/0/109632723504079696349/posts"></li>
                        <li class="odnoklassniki custom-odnoklassniki"></li>
                        <div class="clear"></div>
                    </ul>
				<?php } ?>
                <div class="next_reed">Читайте далее</div>
                 <div class="next-posts-list">
                    <?php getNextPostsDate(); ?>
                </div>
                <div class="clear"></div>
			</div>
            </div>
		</div>
	</div><!-- #primary -->



<?php get_footer(); ?>