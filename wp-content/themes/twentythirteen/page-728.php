<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();

?>
<div class="archive" style="background: none;">

        <div class="container">
            <header class="header-wrapper">
                <div class="button-wrapper">
                    <a href="<?php echo get_permalink(728); ?>" class="info-button info-button__active">
                      Архив
                    </a>
                    <a href="<?php echo get_category_link(7); ?>">
                      <span class="info-button">Календарь мероприятий</span>
                    </a>
                    <a href="<?php echo get_permalink(401); ?>">
                      <span class="info-button">Анонсы</span>
                    </a>
                </div>
                <div class="header-title">
                    <a href="<?php echo get_category_link(2); ?>" class="news-link">
                        <i></i>Новости
                    </a>
                    <div class="second-level-header">Архив</div>
                </div>
            </header>
            <div class="event-page">
                <div class="events-list">
                    <?php
                    $archive = new WP_Query(array(
                        'posts_per_page'  => 8,
                        'paged'           => ( get_query_var('paged') ? get_query_var('paged') : 1),
                        'cat'             => '',
                        'orderby'         => 'date',
                        'order'           => 'DESC',
                        'include'         => '',
                        'exclude'         => '',
                        'meta_key'        => '',
                        'meta_value'      => '',
                        'post_type'       => 'post',
                        'post_mime_type'  => '', // image, video, video/mp4
                        'post_parent'     => '',
                        'post_status'     => 'publish',
                    ));
                     ?>
                    <?php while($archive->have_posts() ) : $archive->the_post(); ?>
                        <div class="news-article">
                            <div class="new-header">
                              <div class="post-date"><?php the_time('d.m.Y'); ?></div>
                              <a href="<?php echo get_permalink(); ?>" class="post-title">
                                <?php the_title(); ?>
                              </a>
                            </div>
                        <div class="post-content">
                            <a class="more-button" href="<?php echo get_permalink(); ?>">детальней</a>
                            <div class="clear"></div>
                        </div>
                      </div>
                    <?php endwhile; ?>
                    <div class="news-article">
                        <?php numeric_posts_nav($archive); ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
                <?php get_sidebar();?>
                <div class="clear"></div> 
            </div>

        </div>
</div>

<?php get_footer(); ?>

