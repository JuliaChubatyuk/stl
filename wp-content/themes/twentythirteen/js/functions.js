/**
 * Functionality specific to Twenty Thirteen.
 *
 * Provides helper functions to enhance the theme experience.
 */

( function( $ ) {
  var body    = $( 'body' ),
      _window = $( window ),
    nav, button, menu;

  nav = $( '#site-navigation' );
  button = nav.find( '.menu-toggle' );
  menu = nav.find( '.nav-menu' );

  /**
   * Adds a top margin to the footer if the sidebar widget area is higher
   * than the rest of the page, to help the footer always visually clear
   * the sidebar.
   */
  $( function() {
    if ( body.is( '.sidebar' ) ) {
      var sidebar   = $( '#secondary .widget-area' ),
          secondary = ( 0 === sidebar.length ) ? -40 : sidebar.height(),
          margin    = $( '#tertiary .widget-area' ).height() - $( '#content' ).height() - secondary;

      if ( margin > 0 && _window.innerWidth() > 999 ) {
        $( '#colophon' ).css( 'margin-top', margin + 'px' );
      }
    }
  } );

  /**
   * Enables menu toggle for small screens.
   */
  ( function() {
    if ( ! nav || ! button ) {
      return;
    }

    // Hide button if menu is missing or empty.
    if ( ! menu || ! menu.children().length ) {
      button.hide();
      return;
    }

    button.on( 'click.twentythirteen', function() {
      nav.toggleClass( 'toggled-on' );
      if ( nav.hasClass( 'toggled-on' ) ) {
        $( this ).attr( 'aria-expanded', 'true' );
        menu.attr( 'aria-expanded', 'true' );
      } else {
        $( this ).attr( 'aria-expanded', 'false' );
        menu.attr( 'aria-expanded', 'false' );
      }
    } );

    // Fix sub-menus for touch devices.
    if ( 'ontouchstart' in window ) {
      menu.find( '.menu-item-has-children > a, .page_item_has_children > a' ).on( 'touchstart.twentythirteen', function( e ) {
        var el = $( this ).parent( 'li' );

        if ( ! el.hasClass( 'focus' ) ) {
          e.preventDefault();
          el.toggleClass( 'focus' );
          el.siblings( '.focus' ).removeClass( 'focus' );
        }
      } );
    }

    // Better focus for hidden submenu items for accessibility.
    menu.find( 'a' ).on( 'focus.twentythirteen blur.twentythirteen', function() {
      $( this ).parents( '.menu-item, .page_item' ).toggleClass( 'focus' );
    } );
  } )();

  /**
   * @summary Add or remove ARIA attributes.
   * Uses jQuery's width() function to determine the size of the window and add
   * the default ARIA attributes for the menu toggle if it's visible.
   * @since Twenty Thirteen 1.5
   */
  function onResizeARIA() {
    if ( 643 > _window.width() ) {
      button.attr( 'aria-expanded', 'false' );
      menu.attr( 'aria-expanded', 'false' );
      button.attr( 'aria-controls', 'primary-menu' );
    } else {
      button.removeAttr( 'aria-expanded' );
      menu.removeAttr( 'aria-expanded' );
      button.removeAttr( 'aria-controls' );
    }
  }

  _window
    .on( 'load.twentythirteen', onResizeARIA )
    .on( 'resize.twentythirteen', function() {
      onResizeARIA();
  } );

  /**
   * Makes "skip to content" link work correctly in IE9 and Chrome for better
   * accessibility.
   *
   * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
   */
  _window.on( 'hashchange.twentythirteen', function() {
    var element = document.getElementById( location.hash.substring( 1 ) );

    if ( element ) {
      if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) {
        element.tabIndex = -1;
      }

      element.focus();
    }
  } );

  /**
   * Arranges footer widgets vertically.
   */
  if ( $.isFunction( $.fn.masonry ) ) {
    var columnWidth = body.is( '.sidebar' ) ? 228 : 245;

    $( '#secondary .widget-area' ).masonry( {
      itemSelector: '.widget',
      columnWidth: columnWidth,
      gutterWidth: 20,
      isRTL: body.is( '.rtl' )
    } );
  }


  
} )( jQuery );

$(document).ready(function(){
  setTimeout(function(){
    $(".third-level-header").dotdotdot();
  }, 400);

  $("[data-view-port-checker-news]").on("mouseenter", function(){

      $(this).find(".project-item-text").dotdotdot();
  });

  var hide_scroll = $("[data-scroll-hide]"),
    flag_chack_offset = true;


  if(hide_scroll[0]){
    hide_scroll.html(hide_scroll.html().replace("<!--", "").replace("-->", ""));

    if(window.innerWidth > 500){
      animationVideo();
    }
    // $(document).on("scroll", checkOffset);
    // checkOffset();
  }

  function animationVideo () {
      $(".video-content-main")
      .addClass("animation-grid")
      .attr("data-view-port-checker", true)
      .viewportChecker({
          removeClassAfterAnimation: false,
          classToAdd: 'visible animated',
          callbackFunction: function(elem, action){
            elem.addClass("past-animation fadeInUp");
          },
          offset: "-70%"
      })
      .find("iframe")
      .each(function(idx, el){
        // задает нужный порядок для анимации блоков
        switch (idx) {
          case 0:
            idx = 1;
            break;
          case 1:
            idx = 2;
            break;
          case 2:
            idx = 3;
            break;
          case 3:
            idx = 4;
            break;
          case 4:
            idx = 8;
            break;
          case 5:
            idx = 5;
            break;
          case 6:
            idx = 6;
            break;
          case 7:
            idx = 7;
            break;
        }

        $(el).attr("data-view-port-checker-news", idx);

      });


      $("article.learn .clause-wrapper > div").each(function(idx, el){
        $(el).attr("data-view-port-checker", true);

        $(el).viewportChecker({
          classToAdd: '',
          callbackFunction: function(elem, action){
            if(action == "add"){
              setTimeout(function(){
                elem.addClass('visible animated fadeInUp');

                setTimeout(function(){
                  elem.addClass("past-animation");
                }, 1000)
              }, (idx * 300));

            }
          },
          offset: "-35%"
        });
      });
    }
  // Реализации анимации при скролле на главной странице
  // Если это моб. расширение анимация не иницализируется

  $.fn.goTo = function() {

    $('[data-view-port-checker]').addClass('animation-disabled ');
    
    $('html, body').animate({
        scrollTop: $(this).offset().top  + 'px'
    }, 'slow');
    return this; // for chaining...
  }


  if(window.innerWidth > 500){

    // Реализет анимацию на главной странице для блока с новостями
    $(".news-grid").attr("data-view-port-checker", true)
    .viewportChecker({
        removeClassAfterAnimation: false,
        classToAdd: 'visible animated',
        callbackFunction: function(elem, action){
          elem.addClass("past-animation fadeInUp");
        }
    });

    // $('[data-view-port-checker]').viewportChecker({
    //  classToAdd: 'visible animated fadeInUp',
    //  callbackFunction: function(elem, action){
    //    elem.addClass("past-animation");
    //  },
    //  offset: "-10%"
    // });

    $('[data-view-port-checker-right]').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      callbackFunction: function(elem, action){
        elem.addClass("past-animation");
      },
      offset: "-10%"
    }).attr("data-view-port-checker", true);

  }else {
    $("[data-view-port-checker]").addClass("past-animation");
    $("[data-view-port-checker-right]").addClass("past-animation");
  }


  $("[name=your-phone]").attr('type', 'tel');



  // function checkOffset(e){
  //   var scroll = window.pageYOffset || document.documentElement.scrollTop;
  //   scroll += window.innerHeight - 200;

  //   if(scroll > hide_scroll.offset().top && flag_chack_offset){
  //     flag_chack_offset = false;
  //     hide_scroll.html(hide_scroll.html().replace("<!--", "").replace("-->", ""));
  //     if(window.innerWidth > 500){
  //         animationVideo();
  //     }
  //   }
  // }








  $('.get-cart').on('click', function() {
      $('.com-card').goTo();
  });

  if($(document.body).hasClass("category-news") && window.innerWidth < 500 ){
    var koff_width = 3;

    $(".news-block-text").each(function(idx, el){
      var text = el.innerHTML;
      text = text.slice(0, text.length / koff_width);
      el.innerHTML = text + "...";
    });
  }

  if($(document.body).hasClass("category-news")){
    setTimeout(function(){
      $(".news-block-title").dotdotdot();
    }, 400);

   $(".news-block").on("mouseenter", function(){

       $(this).find(".news-block-text").dotdotdot();
   })
  }




});

$(document).on("ready", function(){
  $(".page-id-2").addClass('show');
})

