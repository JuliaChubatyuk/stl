var map;
/*create map*/
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 50.43748799999999, lng: 30.5183081},
    zoom: 8
  });
};

initMap();

/*button events: onclick get lat 
and long of adress and display markers */

/*on Kiev*/
$('#kiev').click(function(e){
    var address = 'Shota Rustaveli street, Kiev';
    
 $.getJSON("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false")
            .success(function (data) {
                var coords = data.results[0].geometry.location;
                var center = new google.maps.LatLng(coords.lat, coords.lng);
                map.setCenter(center);
                 new google.maps.Marker({
                      position: center,
                      map: map
                });
            });              
});
/*on Dnipropetrovsk*/
$('#dnepr').click(function(){
    var address = 'center "Menora, Dnipropetrovsk';
    $.getJSON("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false")
            .success(function (data) {
                var coords = data.results[0].geometry.location;
                var center = new google.maps.LatLng(coords.lat, coords.lng);
                map.setCenter(center);
                 new google.maps.Marker({
                    position: center,
                    map: map
                });
            });
});
