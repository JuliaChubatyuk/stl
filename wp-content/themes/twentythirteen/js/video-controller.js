 var result = [];
$(document).ready(function(){
/*get list of videos from stl channel*/
    
    $.ajax({
        url:'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UCKwxDbSf68avvsbs2J_Qi5Q&&maxResults=20&order=date&key=AIzaSyCVI9ECxvHNb1l24_69GeJ2Er4xa_m1yJ4&format=10',
        contentType:'application/json',
        complete:function(args){
            var data = JSON.parse(args.responseText);
            var length = data.items.length;

            $('#prev').attr('data-id', data.items[0].id.videoId);
            $('#next').attr('data-id', data.items[1].id.videoId);


            for(var i = 0; i<length; i++){
                if(data.items[i].id.videoId){

                    if(i == 0) {
                        if($('.video-title').css('position') == 'absolute') {
                            $('#main > div.stl-channel-page > div > article > div.video-container').css('padding-bottom', $('.video-title').height() + 100);
                            $('#main > div.stl-channel-page > div > article > a').css('top', 685 + $('.video-title').height());
                        }
                    }

                    result.push({
                        'id':data.items[i].id.videoId,
                        'title':data.items[i].snippet.title,
                        'channelTitle':data.items[i].snippet.channelTitle
                    });
                }
               
            }
            renderVideos(result);
            showVideoById(data.items[0].id.videoId)
            $('#title').html(data.items[0].snippet.title)
        }
    });
   


    $.ajax({
        url:'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UCKwxDbSf68avvsbs2J_Qi5Q&order=viewCount&maxResults=4&key=AIzaSyCVI9ECxvHNb1l24_69GeJ2Er4xa_m1yJ4&most_popular',
        contentType:'application/json',
        complete:function(args){
            var data = JSON.parse(args.responseText);
            showPopular(data);
        }
    });
    
    $('#prev').click(function(){
       console.log('prev'); 
        showVideoById(this.dataset.id);
         for(var i = 0;i<result.length; i++){
        if(result[i].id === this.dataset.id){ 

            $('#title').html(result[i].title);
        }
    }  
    });
    
    $('#next').click(function(){
        console.log('next'); 
         showVideoById(this.dataset.id);
         for(var i = 0;i<result.length; i++){
            if(result[i].id === this.dataset.id){
                $('#prev').attr('data-id',result[i-1].id);
                $('#next').attr('data-id', result[i+1].id);
                $('#title').html(result[i].title);
                return;
            }
        }  
    });
    
    function showVideoById(id){
         $('#player').html('<iframe src="https://www.youtube.com/embed/'+id+'?enablejsapi=1"></iframe>');
    }
    
     function renderVideos(data){
       var template = $('#video').html();
        Mustache.parse(template);   // optional, speeds up future uses
         
        var rendered = Mustache.render(template, {data: data});
        $('#list').html(rendered);
    }
    
    function showPopular(data){
        $('#slider').children().each(function(i, selector){
            if(data.items[i+1].id.videoId){
                $(selector).html('<iframe src="https://www.youtube.com/embed/'+data.items[i+1].id.videoId+'?enablejsapi=1&height=100"></iframe>');
            }
        });
    };
});

/*init video*/    
    var tag = document.createElement('script');

/*insert iframe api first*/
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

$(document).delegate('.video-wrapper', 'click', function(){

    $('.video-list-wrapper #list div').removeClass('active');
    $(this).addClass('active');


    $('#player').html('<iframe src="https://www.youtube.com/embed/'+this.dataset.id+'?enablejsapi=1"></iframe>');
    for(var i = 0;i<result.length; i++){
        if(result[i].id === this.dataset.id){
            try {
                $('#prev').attr('data-id',result[i-1].id);
                $('#next').attr('data-id', result[i+1].id);
            }catch(e){
                console.info(e);
            }
             $('#title').html(result[i].title);
             if($('.video-title').css('position') == 'absolute') {
                $('#main > div.stl-channel-page > div > article > div.video-container').css('padding-bottom', $('.video-title').height() + 100);
                    console.log($('.video-title').height());
                $('#main > div.stl-channel-page > div > article > a').css('top', 685 + $('.video-title').height());
            }
            return;
        }
    }



});



