<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

    <div class="stl-channel-page">
        <div class="container">
            <article class="main-video-wrapper splice-block-animation">
                <header class="header-wrapper" >
                    <button class="pp w314" onclick="popupShow('popup_donate')">Поддержать проект</button>
                    <div class="second-level-header">STL channel</div>
                    <div class="follow">Подпишись на наш канал в </div>
                    <div class="youtube">Youtube</div>
                </header>
                <div class="video-container" >
                <div id="player"></div>
                </div>
                <div class="video-list">
                    <div class="video-title hide-screen" id="title"><?= empty($videos[0]) ? '' : $videos[0]->fields->title ?></div>
                    <a class="hide-screen" href="https://www.youtube.com/user/STLNEWS770" >
                        <img class="follower" src="/wp-content/themes/twentythirteen//images/follow_youtube.png"/>
                    </a>
                    <div class="controller">
                        <button class="prev" id="prev"></button>
                        <button class="next" id="next"></button>
                        <button class="update"></button>
                    </div>
                    <div class="video-list-wrapper">
                        <div id="list"></div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="video-title hide-mob" id="title" ></div>
                
                <a class=" hide-mob" href="https://www.youtube.com/user/STLNEWS770" >
                    <img class="follower" src="/wp-content/themes/twentythirteen//images/follow_youtube.png"/>
                </a>
                <div class="popular" >
                    <header class="second-level-header">Популярное видео</header>
                    <div class="popular-list" id="slider">
                        <div class="popular-item"></div>
                        <div class="popular-item middle"></div>
                        <div class="popular-item"></div>
                    </div>
                </div>
                <script  id="video" type="x-tmpl-mustache">
                
                {{#data}}
                    <div data-id={{id}} class="video-wrapper" >
                        <div class="video-name">
                            {{title}}
                        </div>
                        <div class="video-channel">
                            {{channelTitle}}
                        </div>
                    </div>
                {{/data}}
                
                </script> 
                <div class="clear"></div>
                </div>
            </article>
        </div>
<script src="//cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.0/mustache.min.js"></script>
<script src='wp-content/themes/twentythirteen/js/video-controller.js'></script>
<?php require_once 'popup_donate.php'; ?>
<?php get_footer(); ?>
