<?php
/**
 * The sidebar containing the secondary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

/*if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
	<div id="tertiary" class="sidebar-container" role="complementary">
		<div class="sidebar-inner">
			<div class="widget-area">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div><!-- .widget-area -->
		</div><!-- .sidebar-inner -->
	</div><!-- #tertiary -->
<?php endif; ?>
*/
?>
        <div id="sidebar">
            <button class="pp custom" onclick="popupShow('popup_donate')">Поддержать проект</button>
            <div class="news-archive-wrapper">
                <?php dynamic_sidebar('sidebar-2')?>
                <div class="news-archive">
                    <div class="news-archive-header">Новости</div>
                    <?php if(!in_category(4,get_the_ID())) { ?>
                       <div class="archive-list">
                            <div class="archive-list-items">
                                <?php recentPostsDate(); ?>
                            </div>
                       </div>
                    </div>
                    <?php } ?>
            </div>
            <div class="clear"></div>
        </div>
<?php require_once 'popup_donate.php'; ?>
