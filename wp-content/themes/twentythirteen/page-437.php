<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<script>
    $(function() {
        $('#slides').slidesjs({
            preload: true,
            preloadImage: 'img/loading.gif',
            generatePagination: false,
            navigation: false ,
            pagination: false,
            width: 604,
            height: 393
        });
    });
</script>
<div style="width: 1229px; margin: 0 auto">
<header>
        <div class="header">
            <div class="second-level-header">STL Teens</div>
            <button class="pp" onclick="popupShow('popup_donate')">Поддержать проект</button>
        </div>
</header>
    <div class="clear"></div><!-- clear -->

    <main>
        <div class="main main-project">
            <img  src="/wp-content/themes/twentythirteen/images/pr-teens-logo.png" alt="pr-stl-logo" class="pr-stl-img_0"/>

            <div  class="pr-stl_text_1 title-font-weight">STL Teens – особый проект для подростков. </div>

            <div  class="pr-stl_text_6">Он появился совсем недавно, поэтому пока не набрал такой популярности. </div>


            <div  class="pr-stl-img_4 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-translatey="70">
                <div id="slides" class="posR">
                    <img src="/wp-content/themes/twentythirteen/img/1/0.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/2.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/3.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/4.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/5.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/6.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/7.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/8.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/9.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/10.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/11.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/12.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/13.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/14.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/15.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/16.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/17.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/18.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/19.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/20.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/21.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/1/22.jpg" alt="" />
                    <a href="#" class="slidesjs-previous slidesjs-navigation"><i class="pr-teens_arrow_left"></i></a>
                    <a href="#" class="slidesjs-next slidesjs-navigation"><i class="pr-teens_arrow_right"></i></a>
                </div>
            </div>

            <img  src="/wp-content/themes/twentythirteen/images/foto5.jpg" alt="foto1" class="pr-stl-img_5 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-translatey="100"/>

            <div  class="pr-stl-text_9 animateme scrollme"
            data-when="view"
            data-from=".5"
            data-to=".1"
            data-opacity="0"
            data-translatey="100">
                <div class="pr-stl-text_10">Суть проекта заключается в проведении мероприятий и различных активностей для молодежи младшего возраста.</div>
            </div>

            <div  class="pr-stl-text_8 animateme scrollme"
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-opacity="0"
            data-translatey="100">Наши встречи дают ребятам возможность больше узнать о еврейских традициях, найти друзей и научиться работать в команде. </div>

            <div  class="pr-stl-text_7 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-opacity="0"
            data-translatey="100">Проект предназначен для участников <br/> 12-17 лет</div>

            <img src="/wp-content/themes/twentythirteen/images/foto6.jpg" alt="foto3" class="pr-stl-img_6 animateme scrollme" 
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-translatey="100"/>
        </div>
        <div class="second-level-header">Другие проекты</div>
    </main>


</div>
<footer class="footer">

    <div class="pr-stl">
        <div class="pr-stl_wrapper">
            <div class="floatL">
                <a href="<?php echo get_permalink(459); ?>"><div class="pr-stl_content pr-stl_content_1"></div></a>
            </div>
            <div class="floatL">
                <a href="<?php echo get_permalink(465); ?>"><div class="pr-stl_content pr-stl_content_2"></div></a>
            </div>
            <div class="floatL">
                <a href="<?php echo get_permalink(463); ?>"><div class="pr-stl_content pr-stl_content_3"></div></a>
            </div>
            <div class="floatL">
                <a href="<?php echo get_permalink(438); ?>"><div class="pr-stl_content pr-stl_content_5"></div></a>
            </div>
        </div>
    </div>
</footer>
<script >
    $(document).ready(function(){
        if(window.innerWidth > 500){
         var script_scroll = $("<script>");
         script_scroll.attr("src", '/wp-content/themes/twentythirteen/js/jquery.scrollme.min.js')
         $(document.body).append(script_scroll);
        }
    });
</script>
<?php require_once 'popup_donate.php'; ?>
<?php get_footer(); ?>