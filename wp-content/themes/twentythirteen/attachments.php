<?php
$page = (int) $_GET['a_page'];
$attachmentPerPage = 10;
$attachments = new Attachments('attachments');

if($attachments->exist()) :
    $files = [];
    while($attachment = $attachments->get()) {
        $file = $attachment;
        $file->url = $attachments->url();
        $files[] = $file;
    }

    $total = $attachments->total();
    $maxpage = ceil($total / $attachmentPerPage);
    if($page < 1) {
        $page = 1;
    }

    if($page > $maxpage) {
        $page = $maxpage;
    }

    $files = array_chunk($files, $attachmentPerPage);
    $files = $files[$page - 1];
    foreach($files as $attachment):
    ?>
        <div class="item-archive">
        <div class="item-date"><?= $attachments->date('d.m.Y') ?></div>
        <div class="item-title"><?= $attachment->fields->title ?></div>
        <a class="item-link" title="<?= strip_tags($attachment->fields->caption) ?>" href="<?= $attachment->url ?>">скачать</a>
        </div>
    <?php
    endforeach;
    if($total > $attachmentPerPage) {
        $link = get_permalink();
        if(strpos($link, '?') === false) {
            $link .= '?';
        }
        else {
            $link .= '&';
        }
        $dots = true;
        ?>
            <div class="post-paging">
                <nav class="navigation" role="navigation">
                    <div class="nav-links">
                        <?php if($page > 1) :?>
                            <a class="nav-prev" href="<?= $link ?>a_page=<?= $page - 1 ?>">&lt;</a>
                        <?php else : ?>
                            <span class="nav-prev" >&lt;</span>
                        <?php endif ?>
                        <?php for($p = 1;$p <= $maxpage; $p++) : ?>
                            <?php if($p < 3
                                OR $p > $maxpage - 2
                                OR ($p > $page - 2 AND $p < $page + 2)
                            ) :
                                $dots = true;
                            ?>
                                <?php if($page <> $p) : ?>
                                    <a class="nav-link" href="<?= $link ?>a_page=<?= $p ?>"><?= $p ?></a>
                                <?php else : ?>
                                    <span class="nav-current" ><?= $p ?></span>
                                <?php endif ?>
                            <?php else : 
                                if($dots) {
                                    echo '...';
                                }
                                $dots = false;
                                endif ?>
                        <?php endfor ?>
                        <?php if($page < $maxpage) : ?>
                            <a class="nav-next" href="<?= $link ?>a_page=<?= $page + 1 ?>">&gt;</a>
                        <?php else : ?>
                            <span class="nav-next" >&gt;</span>
                        <?php endif ?>

                    </div><!-- .nav-links -->
                </nav><!-- .navigation -->
            </div>
        <?php
    }
endif;

 