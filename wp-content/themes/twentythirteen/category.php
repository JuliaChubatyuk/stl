<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>


	<div class="category-page">
	<?php if ( have_posts() ) : ?>

		<?php if(is_category(4)) { ?>
		<header class="lined long-red short-blue">
			<h2><?php printf(single_cat_title( '', false ) ); ?></h2>
		</header>
		<?php } else { ?>
		<header class="lined long-green short-orange">
			<h2><?php printf(single_cat_title( '', false ) ); ?></h2>
		</header>
		<?php } ?>
		<?php if(is_category(4)) { ?>

		<div class="about-list">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="post-item post-<?php the_ID(); ?>">
					<div class="post-image">
						<?php
						if ( has_post_thumbnail() ) {
							echo get_the_post_thumbnail(
								the_ID(),
								array(344,335)
							);
						}
						?>
						<div class="image-inline">&nbsp;</div>
					</div>
					<div class="post-desc">
						<?php echo get_more_excerpt($post); ?>
					</div>
					<div class="post-more">
						<a href="<?php the_permalink(); ?>">детальнее</a>
					</div>
					<div class="clear"></div>
				</div>
			<?php endwhile; ?>
			<div class="clear"></div>
		</div>

		<?php } else { ?>
		<div class="posts-list">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="post-item">
					<div class="post-image">
					<?php
						if ( has_post_thumbnail() ) {
							the_post_thumbnail();
						}
					?>
						<div class="image-inline">&nbsp;</div>
					</div>
					<div class="post-title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>
					<div class="post-date">
						<?php the_time('d.m.Y'); ?>
					</div>
					<div class="post-desc">
						<?php echo wp_trim_excerpt(get_the_excerpt()); ?>
					</div>
					<div class="post-more">
						<a href="<?php the_permalink(); ?>">детальнее</a>
					</div>
				</div>
			<?php endwhile; ?>
			<div class="clear"></div>
		</div>
		<div class="post-paging">
			<?php twentythirteen_paging_nav(); ?>
		</div>
		<?php } ?>
	<?php else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
