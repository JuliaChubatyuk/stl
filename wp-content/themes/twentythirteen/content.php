<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

	<div class="news-article">
        <div class="new-header"> <?php if(is_single())the_post_thumbnail( array(700, 350) );?>
            
            <?php if(!in_category(4,get_the_ID())){ ?>
            
                <?php if(is_single()){ ?>
                    <div class="post-title"><?php the_title(); ?></div>
                    <div class="post-date"><?php the_time('d.m.Y'); ?></div>
                <?php } else if(is_archive()){?>
            
                  <div class="post-date"><?php the_time('d.m.Y'); ?></div>
                  <div class="post-title"><?php the_title(); ?></div>
                <?php } ?>
            
        <?php }?>
        </div>
		<div class="post-content">
            
			<?php if(is_single()){ ?>
			     <?php the_content(); ?>
			<?php } else if(is_archive()) { ?>
			     <a class="more-button" href="<?php the_permalink(); ?>">детальней</a>
			<?php } ?>
			<div class="clear"></div>
		</div>
	</div>
