<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div style="width: 1229px; margin: 0 auto">
    <header>
        <div class="header">
            <div class="second-level-header">STL Университет</div>
            <button class="pp" onclick="popupShow('popup_donate')">Поддержать проект</button>
        </div>
    </header>
    <div class="clear"></div><!-- clear -->
    <main>
        <div class="main main-project">
            <img  src="/wp-content/themes/twentythirteen/images/pr-univ-logo.png" alt="pr-stl-logo" class="pr-stl-img_0"/>

            <div  class="pr-stl_text_0 pr-stl_text_1_border">STL Университет – это новый совместный проект Шиурей Тора Любавич, днепропетровского Университета им. Альфреда Нобеля и Международного гуманитарно-педагогического института «Бейт Хана». </div>

            <img  src="/wp-content/themes/twentythirteen/images/foto15.jpg" alt="foto15" class="pr-univ-img_1"/>

            <img  src="/wp-content/themes/twentythirteen/images/foto16.jpg" alt="foto15" class="pr-univ-img_2"/>

            <div  class="pr-univ_text_2"><span>Для кого:</span><br/> Для еврейских юношей и девушек, которые после окончания 11 класса поступают в днепропетровский «Университет им. Альфреда Нобеля», создана уникальная программа поддержки в получении образования: возможность одновременно получать светское высшее образование и параллельно изучать еврейские традиции и историю.</div>

            <img  src="/wp-content/themes/twentythirteen/images/foto17.jpg" alt="foto16" class="pr-univ-img_3  animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-translatey="90"/>

            <div class="pr-univ-text_3  animateme scrollme"
            data-when="view"
            data-from=".7"
            data-to=".2"
            data-opacity="0"
            data-translatey="100">Участникам программы полностью оплачивают обучение, предоставляют кошерное питание и возможность преимущественного права на участие в мероприятиях Шиурей Тора. Кроме учебы в университете, участники программы посещают занятия STARS (2-3 раза в неделю по 2 часа).<br/><br/>Иногородние студенты селятся в бесплатном общежитии с кошерным питанием и опытными кураторами.</div>



        </div>
        <div class="second-level-header">Другие проекты</div>
    </main>
    <div class="clear"></div><!-- clear -->
</div>
    <footer class="footer">

        <div class="pr-stl">
            <div class="pr-stl_wrapper">
                <div class="floatL">
                    <a href="<?php echo get_permalink(459); ?>"><div class="pr-stl_content pr-stl_content_1"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(465); ?>"><div class="pr-stl_content pr-stl_content_2"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(437); ?>"><div class="pr-stl_content pr-stl_content_4"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(438); ?>"><div class="pr-stl_content pr-stl_content_5"></div></a>
                </div>

            </div>
        </div>
    </footer>

    <script >
        $(document).ready(function(){
            if(window.innerWidth > 500){
             var script_scroll = $("<script>");
             script_scroll.attr("src", '/wp-content/themes/twentythirteen/js/jquery.scrollme.min.js')
             $(document.body).append(script_scroll);
            }
        });
    </script>

<?php require_once 'popup_donate.php'; ?>
<?php get_footer(); ?>