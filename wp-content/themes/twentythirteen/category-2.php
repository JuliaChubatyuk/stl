<?php get_header(); ?>
 <div class="container">
     <div class="main-page-news">
         <header class="header-wrapper">
            <div class="button-wrapper">
                <!-- 
                <?php $args = array(
                        'type'            => 'yearly',
                        'limit'            => 1,
                    );
                    echo wp_get_archives($args); ?>
                 -->
                <a href="<?php echo get_category_link(7); ?>">
                    <span class="info-button">Календарь мероприятий</span>
                </a>
                <a href="<?php echo get_permalink(401); ?>">
                    <span class="info-button">Анонсы</span>
                </a>
            </div>
            <div class="header-title">
                <span class="second-level-header">Новости</span>
            </div>
        </header>
         <article class="news-grid main-project">
             <div class="news-blocks">
                    <?php global $wp_query; ?>
                    <?php $showjoin = false; ?>
                    <?php $tagopen = false; ?>
                    <?php $newsNum = 0; ?>
                    <?php $img1 = get_template_directory_uri().'/images/program.png'; ?>
                    <?php $img2 = get_template_directory_uri().'/images/vipusk.png'; ?>
                    <?php $img3 = get_template_directory_uri().'/images/seminar.png'; ?>
                    <?php $images = array(1 => $img1, 2 => $img2, 3 => $img3, 4 => $img3, 5 => '', 6 => '', 7 => $img1, 8 => $img2, 9 => '', 10 => '', 11 => $img2, 12 => $img2, 13 => ''); ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <a href="<?php echo get_permalink(); ?>">
                            <section  class="news-block news-block-<?= ++$newsNum; ?>"
                                <?php if(!empty($images[$newsNum])) : ?>
                                data-new-image
                               
                            style="background: url(<?php echo catch_that_image(get_post(), $images[$newsNum]) ?>);background-position: center; background-size: cover;"

                                <?php endif ?>

                            >
                                <?php /*if(!empty($images[$newsNum])) : ?>
                                    <img src="<?php echo catch_that_image(get_post(), $images[$newsNum]) ?>" />
                                <?php endif*/ ?>
                                <section class="news-block-wrap">
                                    <section class="news-block-title">
                                    
                                     <?php  switch($newsNum) {
                                        case 1:
                                        ?><?= the_title(); ?>
                                        <?php
                                        break;
                                        case 2:
                                        ?>
                                            <?= the_title(); ?>
                                        <?php
                                        break;
                                        case 3:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 4:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 5:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 6:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 7:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 8:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 9:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 10:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 11:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                        case 12:
                                        ?>
                                            <?= the_title() ?>

                                        <?php
                                        break;
                                        case 13:
                                        ?>
                                            <?= the_title() ?>
                                        <?php
                                        break;
                                      };  ?>
                                    </section>
                                    <p class="news-block-date"><?= get_the_date(); ?></p>
                                    <?php switch($newsNum) {
                                        case 1:
                                        ?>
                                        <section class="news-block-text"><?= get_the_content() ?></section>
                                    <?php
                                    break;
                                    case 2:
                                    ?>
                                        <section class="news-block-text"><?= get_the_content() ?></section>
                                    <?php
                                    break;
                                    case 3:
                                    ?>
                                        <section class="news-block-text"><?= get_the_content(); ?></section>
                                    <?php
                                    break;
                                    case 4:
                                    ?>
                                        <section class="news-block-text"><?= get_the_content() ?></section>
                                    <?php
                                    break;
                                    case 7:
                                    ?>
                                        <section class="news-block-text"><?= get_the_content() ?></section>
                                    <?php
                                    break;
                                    case 8:
                                    ?>
                                        <section class="news-block-text"><?= get_the_content() ?></section>
                                    <?php
                                    break;
                                    case 11:
                                    ?>
                                        <section class="news-block-text"><?= get_the_content() ?></section>
                                    <?php
                                    break;
                                    case 12:
                                    ?>
                                        <section class="news-block-text"><?= get_the_content() ?></section>
                                    <?php
                                    break;
                                    }; ?>
                                </section>
                            </section>
                        </a>
                        <?php switch($newsNum) {
                            case 1:
                                ?>
                                <a  class="news-block news-block-<?= $newsNum; ?>-1" href="<?php echo get_category_link(7); ?>">
                                    <div class="check-event rounded-item">
                                        <img class="mark" src="<?= get_template_directory_uri().'/images/calendar.png' ?>">
                                        <div class="fourth-level-header">Выбрать ближайшее <br> мероприятие</div>
                                    </div>
                                </a>
                                <?php
                                break;
                            case 2:
                                ?>


                                <div class="clear"></div>
                                <a  class="news-block news-block-<?= $newsNum; ?>-1" href="#" onclick="popupShow('popup_donate'); return false">
                                    <div class="support small-item">
                                        <div class="fourth-level-header"> Поддержать<br> проект</div>
                                        <img src="<?= get_template_directory_uri().'/images/time.png' ?>"/>
                                    </div>
                                </a>
                                <a  class="news-block news-block-<?= $newsNum; ?>-2" href="<?php echo get_permalink(401); ?>">
                                    <div class="learn small-item">
                                        <div class="fourth-level-header">Выучить<br> сегодня </div>
                                        <img src="<?= get_template_directory_uri().'/images/learn.png' ?>"/>
                                    </div>
                                </a>
                                <?php
                                break;
                            case 3:
                                $showjoin = true;
                                ?>
                                <a  class="news-block news-block-<?= $newsNum; ?>-1" href="#" onclick="popupShow('popup_signup'); return false">
                                    <div class="join small-item">
                                        <div class="fourth-level-header"> Записаться на<br> программу</div>
                                        <img src="<?= get_template_directory_uri().'/images/join.png' ?>"/>
                                    </div>
                                </a>
                                <?php if($wp_query->current_post + 1 < $wp_query->post_count):
                                $tagopen = true;
                                ?>
                                    <div class="news-block news-block-<?= $newsNum; ?>-2" >
                                <?php endif ?>
                                <?php
                                break;
                            case 6:
                                $tagopen = false;
                                ?>
                                </div>
                                <?php
                                break;
                            case 7:
                                ?>
                                <?php if($wp_query->current_post + 1 < $wp_query->post_count):
                                $tagopen = true;
                                ?>
                                <article class="news-block news-block-<?= $newsNum; ?>-1" >

                                <?php endif ?>
                                <?php
                                break;
                            case 9:
                                $tagopen = false;
                                ?>
                                </article>
                                <?php
                                break;
                            }; ?>
                    <?php endwhile ?>
                    <?php if($tagopen): ?></article><?php endif ?>
                    <?php if(!$showjoin): ?>
                        <?php for($i = $newsNum+1; $i<= 3; $i++): ?>
                            <div  class="news-block news-block-<?=$i ?>">
                            </div>
                            <?php switch($i) {
                                case 2:
                                    ?>
                                    <div class="clear"></div>
                                    <a  class="news-block news-block-<?= $i; ?>-1" href="#" onclick="popupShow('popup_donate'); return false">
                                        <div class="support small-item">
                                            <div class="fourth-level-header"> Поддержать<br> проект</div>
                                            <img src="<?= get_template_directory_uri().'/images/time.png' ?>">
                                        </div>
                                    </a>
                                    <a  class="news-block news-block-<?= $i; ?>-2" href="<?php echo get_permalink(401); ?>">
                                        <div class="learn small-item">
                                            <div class="fourth-level-header">Выучить<br> сегодня </div>
                                            <img src="<?= get_template_directory_uri().'/images/learn.png' ?>">
                                        </div>
                                    </a>
                                    <?php
                                    break;
                                case 3:
                                    ?>
                                    <a  class="news-block news-block-<?= $i; ?>-1" href="#" onclick="popupShow('popup_signup'); return false">
                                        <div class="join small-item">
                                            <div class="fourth-level-header"> Записаться на<br> программу</div>
                                            <img src="<?= get_template_directory_uri().'/images/join.png' ?>">
                                        </div>
                                    </a>
                                    <?php
                                    break;
                                }; ?>
                        <?php endfor ?>
                    <?php endif ?>
                    <div class="clear"></div>
                    <?php numeric_posts_nav() ?>
                </div>
        </article>
    </div>
</div>
<?php require_once 'popup_donate.php'; ?>
<?php require_once 'popup_signup.php'; ?>
<?php get_footer();?>

