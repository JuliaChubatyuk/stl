<?php // используются скрипты ?>
<?php require_once 'popup_donate.php'; ?>

<div id="popup_signup" class="popup-overlay" style="display: none">
    <div class="popup-wrapper">
        <div class="fog"></div>
        <div class="popup-container float-end background_1">
            <div class="popup_close" onclick="popupHide()"></div>
            <div class="popup-header">
            </div>
            <div class="popup-content_1">
                <div class="second-level-header">Запишись на программу</div>
                <div class="descript_1">Оставьте нам, пожалуйста, информацию о себе, чтобы мы могли с вами связаться</div>
                <?php echo do_shortcode('[contact-form-7 id="490" title="Запишись на программу"]'); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[type=radio] + span').click(function(){
            $(this).prev().click();
        });
    })
</script>