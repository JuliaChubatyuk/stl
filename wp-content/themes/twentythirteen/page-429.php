<?php get_header();?>
<div class="container splice-block-animation">
    <div class="lessons-page">
        <header class="header-wrapper" >
            <div class="button-wrapper">
                <a href="/?page_id=728">
                    <span class="info-button">Архив</span>
                </a>
                <a href="/?page_id=401"><div class="info-button">На этой неделе</div></a>
            </div>
            <div class="header-title">
                <div class="second-level-header">Текстовые уроки</div>
            </div>
        </header>
        <div class="lessons-content">
             <div class="lessons-thumbnail">
                <div class="image-wrapper" >
                    <img src="<?php echo get_template_directory_uri(); ?>/images/big-text-lessons.png"/>
                </div>
                <div class="title-wrapper" >
                    <div class="lessons-title">Текстовые уроки<br /> Шауля-Айзека Андрущака</div>
                    <div class="learn">учить уроки</div> 
                </div>
            </div>
            <div class="lessons-list">
                <?php while(have_posts()):
                    the_post(); ?>
                        <?php remove_filter('the_content', 'wpautop'); ?>
                        <?php the_content(); ?>
                        <?php include_once (ABSPATH.'/wp-content/themes/twentythirteen/attachments.php'); ?>
                <?php endwhile; ?>
            </div>
        </div>
        <?php get_sidebar(); ?>
    </div>
</div>

<?php get_footer();?>