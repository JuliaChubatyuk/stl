<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();

		 if ( have_posts() ) : ?>

        <div class="container">
            <header class="header-wrapper">
                <div class="button-wrapper">
                    <a href="#" class="info-button info-button__active">
                      Архив
                    </a>
                    <a href="<?php echo get_category_link(7); ?>">
                      <span class="info-button">Календарь мероприятий</span>
                    </a>
                    <a href="<?php echo get_permalink(401); ?>">
                      <span class="info-button">Анонсы</span>
                    </a>
                </div>
                <div class="header-title">
                    <a href="<?php echo get_category_link(2); ?>" class="news-link">
                        <i></i>Новости
                    </a>
                    <div class="second-level-header">Архив</div>
                </div>
            </header>
            <div class="event-page">
                <div class="events-list">
                    <?php /* The loop */ ?>
                    <?php while(have_posts() ) : the_post(); ?>

                        <?php get_template_part('content', get_post_format()); ?>
                    <?php endwhile; ?>
                    <div class="news-article">
                        <?php numeric_posts_nav(); ?>
                    </div>
                </div>
                <?php get_sidebar();?>
                <div class="clear"></div> 
            </div>

        </div>
		<?php endif; ?>
<?php get_footer(); ?>

