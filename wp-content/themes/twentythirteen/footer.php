<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
			
		</div><!-- #main -->
		<footer id="colophon" class="site-footer1" role="contentinfo">
            <div class="colored-line">
		<div class="blue-line line"></div>
		<div class="red-line line"></div>	
		<div class="yellow-line line"></div>	
		<div class="green-line line"></div>		
		</div>
			
		<article class="about-us container"> 
			<div class="contacts">
				<div class="fifth-level-header">Контакты для связи</div>
				<div class="kyiv">
					<div class="pointer"></div> 
					<div class="representation-header">Киевское представительство</div>
					<div class="representation-text">ул Шота Руставели, 13<br>синагога Бродского<br>тел. 067 567 77 42,<div class="telephone">044 235 90 82</div>shitokiev@gmail.com</div>
				</div>
				<div class="dnepr">
					<div class="pointer"></div> 
					<div class="representation-header">Днепропетровское представительство</div>	
					<div class="representation-text">ул Шолом-Алейхема, 4/26<br>центр &#171 Менора &#187<br>тел. 067 525 27 70,<div class="telephone">050 424 27 70,</div><div class="telephone">093 307 37 70</div></div>		
				</div>
	
			</div>
			<div class="stl">
				<div class="fifth-level-header">STL - что это такое</div>
				<img class="logo"  src="<?php echo get_template_directory_uri(); ?>/images/mini-logo.png"/>
				<div class="stl-text">ШИУРЕЙ ТОРА ЛЮБАВИЧ &#8212 лидер неформального <br> изучения еврейских традиций не только в Украине, <br> но и за пределами страны</div>  
				<div class="social-networks">
					<div class="fifth-level-header">Мы здесь</div>
					<a href="https://www.facebook.com/stlubavich" class="facebook link"></a>
					<a href="https://vk.com/stlubavich" class="vkonakte link"></a>
					<a href="https://www.youtube.com/user/STLNEWS770" class="you-tube link"></a>				
				 </div>
                <div class="clear"></div>
			</div>
		</article>
			<div class="footer-color-line">
				<div class="blue-line line">
					<div class="copyright">STL &#169 2015</div>	
				</div>
				<div class="red-line line"></div>
				<div class="yellow-line line"></div>
				<div class="green-line line"></div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
