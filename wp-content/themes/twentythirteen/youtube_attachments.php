<?php

$attachments = new Attachments('youtube_attachments');
$videos = [];
$popular = [];
$glavas = [];
$datehs = [];
$dategs = [];
if($attachments->exist()) :
    while($attachment = $attachments->get()) {
        preg_match('|v=([^&]*)|is', $attachment->fields->link, $match);
        $attachment->fields->id = $match[1];
        $videos[] = $attachment;
        $glavas[] = $attachment->fields->glava;
        $datehs[] = $attachment->fields->dateh;
        $dategs[] = $attachment->fields->dateg;
    }

    sort($glavas);
    sort($datehs);
    sort($dategs);

    $glavas = array_unique($glavas);
    $datehs = array_unique($datehs);
    $dategs = array_unique($dategs);

    $popular = $videos;
    shuffle($popular);
    $popular = array_chunk($popular, 3);
    $popular = $popular[0];
endif;
