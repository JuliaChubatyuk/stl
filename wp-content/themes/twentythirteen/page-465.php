<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
    <script>
        $(function() {
            $('#slides').slidesjs({
                preload: true,
                preloadImage: 'img/loading.gif',
                generatePagination: false,
                navigation: false ,
                pagination: false,
                width: 594,
                height: 393
            });
        });
    </script>
<div style="width: 1229px; margin: 0 auto">
    <header>
        <div class="header">
            <div class="second-level-header">STL Stars Lubavich</div>
            <button class="pp" onclick="popupShow('popup_donate')">Поддержать проект</button>
        </div>
    </header>
    <div class="clear"></div><!-- clear -->

    <main>
        <div class="main main-project">
            <img   src="/wp-content/themes/twentythirteen/images/pr-stars-logo.png" alt="pr-stl-logo" class="pr-stl-img_0 animateme scrollme"/>

            <div class="pr-stl_text_0 ">Проект Stars Lubavich  - это наш просветительский проект, посвященный углубленному изучению Торы. </div>

            <div  class="pr-stars_text_2 ">Как правило, все участники проекта учатся или работают. Тем не менее, они хотят получать новые знания об иудаизме в целом, еврейских традициях и культурных ценностях. </div>


            <div   class="pr-stars-img_1">
                <div id="slides" class="posR">
                    <img src="/wp-content/themes/twentythirteen/img/2/0.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/2/2.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/2/3.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/2/4.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/2/5.jpg" alt="" />
                    <img src="/wp-content/themes/twentythirteen/img/2/6.jpg" alt="" />
                    <a href="#" class="slidesjs-previous slidesjs-navigation"><i class="pr-stars_arrow_left"></i></a>
                    <a href="#" class="slidesjs-next slidesjs-navigation"><i class="pr-stars_arrow_right"></i></a>
                </div>
            </div>



            <img   src="/wp-content/themes/twentythirteen/images/foto13.jpg" alt="foto1" class="pr-stars-img_2 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-translatey="90"/>

            <div   class="pr-stars_text_3 animateme scrollme"
            data-when="view"
            data-from=".7"
            data-to=".1"
            data-opacity="0"
            data-translatey="100">В связи с этим, наш проект был реализован в виде еженедельных вечерних занятий с опытными наставниками, которые читают лекции по профильному курсу.</div>

            <div   class="pr-stars-text_4 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-opacity="0"
            data-translatey="100">Также для участников Stars Lubavich есть возможность посещения лагерей, семинаров и различных мероприятий. </div>

            <div   class="pr-stars-text_1 animateme animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".1"
            data-opacity="0"
            data-translatey="50">Рассчитан наш проект на молодежь<br/>возрастом 18 – 28 лет.</div>

            <img   src="/wp-content/themes/twentythirteen/images/foto14.jpg" alt="foto3" class="pr-stars-img_3 animateme scrollme"
            data-when="view"
            data-from=".6"
            data-to=".3"
            data-translatey="50"/>
        </div>
        <div class="second-level-header">Другие проекты</div>
    </main>
</div>

<footer class="footer">

        <div class="pr-stl">
            <div class="pr-stl_wrapper">
                <div class="floatL">
                    <a href="<?php echo get_permalink(459); ?>"><div class="pr-stl_content pr-stl_content_1"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(463); ?>"><div class="pr-stl_content pr-stl_content_3"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(437); ?>"><div class="pr-stl_content pr-stl_content_4"></div></a>
                </div>
                <div class="floatL">
                    <a href="<?php echo get_permalink(438); ?>"><div class="pr-stl_content pr-stl_content_5"></div></a>
                </div>
            </div>
        </div>
    </footer>
    <script >
        $(document).ready(function(){
            if(window.innerWidth > 500){
             var script_scroll = $("<script>");
             script_scroll.attr("src", '/wp-content/themes/twentythirteen/js/jquery.scrollme.min.js')
             $(document.body).append(script_scroll);
            }
        });
    </script>
<?php require_once 'popup_donate.php'; ?>
<?php get_footer(); ?>