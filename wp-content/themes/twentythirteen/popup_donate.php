<?php ?>
<script type="text/javascript">
    var amount = 1;
    var name = '';

    function testInput() {
        var fn = $('[name="FirstName"]').val();
        var sn = $('[name="SecondName"]').val();
        var am = $('[name="DonateSum"]').val();

        var reg = new RegExp("^[A-Za-zА-Яа-я їіІЇёЁ-]{2,100}$");
        if(!reg.test(fn)) {
        window.alert('Имя должно содержать от 2 букв');
        return false;
        }
        if(!reg.test(sn)) {
        window.alert('Фамилия должна содержать от 2 букв');
        return false;
        }
        reg = new RegExp("^[1-9][0-9]*$");
        if(!reg.test(am)) {
        window.alert('Сума должна быть числом');
        return false;
        }

        return true;
    }

    function popupHide() {
        document.getElementsByTagName('body')[0].style.overflow = 'inherit';
        var ovf = document.getElementsByClassName('popup-overlay');
        for (i in ovf) {
            if (ovf[i].style)
                ovf[i].style.display = 'none';
        }
        $('[id^="popup_"]').css('display', 'none');
    }
    function popupShow(id) {
        if(id == 'popup_liqpay') {
            if(!testInput()) {
                return false;
            }

            name = $('[name="FirstName"]').val() + ' ' + $('[name="SecondName"]').val();
            amount = parseInt($('[name="DonateSum"]').val());
            if(amount < 1) {
                amount=1;
            }
            $('#donate-amount').text(amount + " UAH.");
        }


        popupHide();
        document.getElementsByTagName('body')[0].style.overflow = 'hidden';
        $('#'+id).css('display', 'block');
    }

    function payByLiqPay() {
         console.log('start');
        $.ajax({
            method: "POST",
            url: "/wp-content/themes/twentythirteen/LiqPayDonate.php",
            data: { DonateSum: amount, from: document.title, name: name }
        })
        .done(function( msg ) {
            if(!msg) {
                return;
            }

            $('#donate-form').html(msg);
            $('#donate-form form').submit();
        });
    }

    $(document).keydown(function(e){
       var code = e.keyCode || e.which;
       if(code == 27) {
            popupHide();
       }
    });
</script>
        <div id="popup_donate" class="popup-overlay" style="display: none">
            <div class="popup-wrapper">
                <div class="fog"></div>
                <div class="popup-container float-end background_3">
                    <div class="popup_close" onclick="popupHide()"></div>
                    <div class="popup-header">
                    </div>
                    <div class="popup-content_1">

                        <div class="second-level-header">Поддержать проект</div>
                        <div class="pp_otstup_forma">

                            <div class="name-fields">
                                <span class="wpcf7-form-control-wrap your-name"><b>Имя</b><br/><input type="text" name="FirstName" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Введите имя"></span>
                            </div>
                            <div class="secondname-fields">
                                <span class="wpcf7-form-control-wrap your-name"><b>Фамилия</b><br/> <input type="text" name="SecondName" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Введите фамилию"></span>
                            </div>
                            <div class="email-field">
                                <span class="wpcf7-form-control-wrap your-email"><b>Сумма</b><br/> <input type="tel" name="DonateSum" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Введите сумму"></span>
                            </div>
                            <button class="get-button get-button_1" onclick="popupShow('popup_liqpay')">Поддержать проект</button>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="popup_liqpay" class="popup-overlay"  style="display: none">
            <div class="popup-wrapper">
                <div class="fog"></div>
                <div class="popup-container float-end background_3">
                    <div class="popup_close" onclick="popupHide()"></div>
                    <div class="popup-header">
                    </div>
                    <div class="popup-content_1">
                        <div class="second-level-header">Поддержать проект</div>
                        <div class="pp_otstup_forma">
                            <div class="summa-fields">
                                <span class="wpcf7-form-control-wrap your-name payment_text">Сумма платежа: </span>
                                <span class="data_form padL60" id="donate-amount">100 UAH.</span>
                            </div>
                            <div class="znachenie-fields">
                                <span class="wpcf7-form-control-wrap your-name payment_text">Назначение платежа:</span>
                                <span class="data_form padL12">Поддержка проекта</span>
                            </div>



                            <button class="get-button get-button_1" onclick="payByLiqPay();">Перейти к Liqpay</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<div style="display:none;" id="donate-form">
<?php require_once ABSPATH.'/wp-content/themes/twentythirteen/LiqPayDonate.php'; ?>
</div>