<?php get_header(); ?>
    <div class="container">
        <div class="hitas-page">
            <header class="header-wrapper">
                <div class="button-wrapper">
                   <a href="/?page_id=728">
                       <span class="info-button">Архив</span>
                   </a>
                    <a href="<?php echo get_permalink(401); ?>"><div class="info-button">На этой неделе</div></a>
                </div>
                <div class="header-title">
                    <div class="second-level-header">Научись</div>
                </div>
            </header>

            <div class="learn-content">
                <div class="learn-thumbnail">
                    <div class="image-wrapper">
                        <img src="/wp-content/themes/twentythirteen/images/hitas_img.jpg"/>
                    </div>
                    <div class="title-wrapper">
                        <div class="learn-title">Ежедневное <br />изучение ХИТАС</div>
                        <a href="<?php echo get_permalink(406); ?>"><div class="learnlearn">выучи</div></a>
                    </div>
                </div>

            </div>
            <div class="learn-content">
                <div class="learn-thumbnail">
                    <div class="image-wrapper">
                        <img src="/wp-content/themes/twentythirteen/images/lessons_img.jpg"/>
                    </div>
                    <div class="title-wrapper">
                        <div class="learn-title">Текстовые уроки <br />Шауля-Айзека Андрущака</div>
                        <a href="<?php echo get_permalink(429); ?>"><div class="learnlearn">прочитай</div></a>
                    </div>
                </div>

            </div>
            <div class="learn-content">
                <div class="learn-thumbnail">
                    <div class="image-wrapper">
                        <img src="/wp-content/themes/twentythirteen/images/so_vrem.jpg"/>
                    </div>
                    <div class="title-wrapper">
                        <div class="learn-title">Видео уроки<br />
                            "В ногу со временем"</div>
                        <a href="<?php echo get_permalink(444); ?>"><div class="learnlearn">посмотри</div></a>
                    </div>
                </div>

            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>