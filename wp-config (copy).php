<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'stl');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']Dz2Eqw){rIj7H }Eu]3+zI-ASb<Q`K^J-MM$8J4B45v~us1tk__5ORu_[|ws+|n');
define('SECURE_AUTH_KEY',  '[oUku0RKr=Yu}swd?^jFSM)zEA d@q6[u|]5ZmYiB{w[3v>kQwRH#s*jz`k:}:Z,');
define('LOGGED_IN_KEY',    'W!^o4-2]fwE<FJ4De1f!S8j7)d5-dp6j1psChc8S+!!P$[W!muA-F$D~>[C~]6,%');
define('NONCE_KEY',        '8o([EI?IuuP7#]^^2nz;%k+s$aj-wUD9p.`Jsa[np/tRM,E1jsyP:d-Xy,%v%=d3');
define('AUTH_SALT',        'Bzx6#`|q=`>)-L>%h?x4~G.u7%kAG6Rk%!Y%`Oo6cB_YWQXokCZcg7m6kfcM$fP$');
define('SECURE_AUTH_SALT', '=c26]r)SQ$O<aIr5OYY&d_jl[8x<bc#N;QZv52GaMAdbF|AK`^bC3_7Bd$=nUw|K');
define('LOGGED_IN_SALT',   '!}!Z|Y<lkv1QTvMV;- LxauJxQ2e|:^419qE9G-|{20Ox)mT@=bKTDL]KUc|U<&C');
define('NONCE_SALT',       '_HwI]&NKyb{IKRT3tX.I(jOA>W<5( 3qp:Sez6^TPAM5BR9b0U|;N>,(NWbvX?GZ');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'stl_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
